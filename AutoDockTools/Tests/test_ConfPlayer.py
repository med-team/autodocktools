##########################################################################
#   Authors: Sowjanya Karnati,Ruth Huey
#
#
##########################################################################

# $Id: test_ConfPlayer.py,v 1.7 2008/10/03 20:49:22 sargis Exp $
import unittest
import AutoDockTools.ConfPlayer
from AutoDockTools.ConfPlayer import ConformationPlayer
mv = None
ct = 0 
totalCt =14
class ConfPlayerBaseTest(unittest.TestCase):
    
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(withShell=False,logMode='no',trapExceptions=False)
            mv.loadModule('autotorsCommands', 'AutoDockTools')
            mv.loadModule('autogpfCommands', 'AutoDockTools')
            mv.loadModule('autodpfCommands', 'AutoDockTools')
            #8/18:
            mv.loadModule('autoanalyzeCommands', 'AutoDockTools')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
            
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()


    def tearDown(self):
        """
        clean-up
        """
        #print 'in gpf tearDown'
        global ct, totalCt
        #reset docked
        from AutoDockTools.Docking import Docking
        self.mv.docked = None
        for key in self.mv.dockings.keys():
            self.mv.ADanalyze_deleteDLG(key)
        self.mv.dockings = {}
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m) 
        ct = ct + 1
        #print 'ct =', ct
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv    
    
    
    
    
class ConfPlayerTests(ConfPlayerBaseTest):

    
    def test_confplayer_play_play_reverse(self):
        """tests ConfPlayer play,playrev buttons:
        """
        #read docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling confplayer
        player =ConformationPlayer(mol =self.mv.Mols[0],docking = self.mv.dockings['ind.dlg'],vf  = self.mv,sequenceList = self.mv.docked.ch.conformations)
        player.Play_cb()
        self.assertEqual(player.currentFrameIndex,10)
        player.PlayRev_cb()
        self.assertEqual(player.currentFrameIndex,0)
        player.Close_cb()

    def test_confplayer_play_go_to_start(self):
        """tests ConfPlayer go to Start button:"""
        #read docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling player
        player =ConformationPlayer(mol =self.mv.Mols[0],docking = self.mv.dockings['ind.dlg'],vf  = self.mv,sequenceList = self.mv.docked.ch.conformations)
        player.Play_cb()
        self.assertEqual(player.currentFrameIndex,10)
        player.GoToStart_cb()
        self.assertEqual(player.currentFrameIndex,0)
        player.Close_cb()
        
    def test_confplayer_play_go_to_end(self):
        """tests ConfPlayer go to end button:
        """
        #read ldocking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling player
        player =ConformationPlayer(mol =self.mv.Mols[0],docking = self.mv.dockings['ind.dlg'],vf  = self.mv,sequenceList = self.mv.docked.ch.conformations)
        player.PlayRev_cb()
        self.assertEqual(player.currentFrameIndex,0)
        player.GoToEnd_cb()
        self.assertEqual(player.currentFrameIndex,10)
        player.Close_cb()
    
    def test_confplayer_fast_forward_cb_reverse_cb(self):
        """tests ConfPlayer forward,reverse buttons:
        """
        #read docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling player
        player =ConformationPlayer(mol =self.mv.Mols[0],docking = self.mv.dockings['ind.dlg'],vf  = self.mv,sequenceList = self.mv.docked.ch.conformations)
        player.FastForward_cb()
        self.assertEqual(player.currentFrameIndex,10)
        player.FastReverse_cb()
        self.assertEqual(player.currentFrameIndex,0)
        player.Close_cb()
    
    
    def test_confplayer_build_hbonds(self):
        """tests ConfPlayer Build H bonds button:
        """
        #read docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.ADanalyze_readMacromolecule('hsg1.pdbqs', log=0)
        self.mv.colorByMolecules("ind_out;hsg1", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling player
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        player.SetState_cb(1)
        #calling setplay options widget
        player.SetMode_cb()
        player.buildHBondVar.set(1)
        player.buildHBonds()
        c = self.mv.showHBonds
        c.showDistLabels.set(1)
        c.showDistances()
        x = len(mv.allAtoms.get(lambda x: hasattr(x, 'hbonds')).name)/3.
        #tests no.of hydrogen bonds formed  = dlabs displayed
        self.assertEqual(int(round(x)),len(c.dlabs))
        self.mv.showHBonds.dismiss_cb()
        player.cancelPlayMode_cb()
        player.Close_cb()
        
        
    def test_confplayer_show_info(self):
        """tests ConfPlayer show info button:
        """
        #read docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling player
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        #calling setplay options widget
        player.SetMode_cb()
        player.nextFrame(1)
        player.showStatsVar.set(1)
        player.showStats()
        #player.statsForm.root.wait_visibility(player.statsForm.root)
        #tests conformation info widget is opened
        self.assertEqual(player.statsForm.root.winfo_ismapped(),1)
        player.cancelPlayMode_cb()
        player.Close_cb()
        player.CloseStats_cb()
        
    def test_confpalyer_show_conflist(self):
        """tests ConfPlayer show conf list:
        """
        #read docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling player
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        #calling setplay options widget
        player.SetMode_cb()
        player.showListVar.set(1)
        player.showStatesList()
        #player.playModeForm.form3.root.wait_visibility(player.playModeForm.form3.root)
        #tests opens choose conformation widget is opened
        self.assertEqual(player.playModeForm.form3.root.winfo_ismapped(),1)
        player.showListVar.set(0)
        player.showStatesList()
        player.cancelPlayMode_cb()
        player.Close_cb()
        
    def test_confplayer_make_rms_ref(self):
        """tests ConfPlayer make rms ref:
        """
        #read docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        #calling player
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        #calling setplay options widget
        player.nextFrame(1)
        player.SetMode_cb()
        player.MakeRef_cb()
        player.showStatsVar.set(1)
        player.showStats()
        from string import split
        #tests clrms is 0.0 for that conformation ,when make rms ref button clicked
        label_wid = player.statsForm.widgetID_entries.values()[0]['widget']
        text = label_wid.cget('text')
        self.assertEqual(text.split()[-4],'clRMS=0.0')
        #self.assertEqual(split(split(str(player.statsForm.widgetID_entries.values()[0]),',')[8],'\\n')[5],'clRMS=0.0')
        player.cancelPlayMode_cb()
        player.Close_cb()
        player.CloseStats_cb()
        
    def test_confplayer_choose_rms_ref(self):
        """tests ConfPlayer choose rms ref:
        """
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        #calling setplay options widget
        player.SetMode_cb()
        player.SetRMSRef_cb()
        #setting rmsref mol 
        player.chooser.ipf.entryByName['Molecule']['widget'].selectItem(0)
        player.rmsRefMolecule_cb()
        self.assertEqual(player.rmsRef.name,'ind_out')
        player.cancelPlayMode_cb()
        player.Close_cb()
        
    def test_confplayer_play_mode(self):
        """tests ConfPlayer play mode:
        """
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        #calling setplay options widget
        player.SetMode_cb()
        player.buildPlayModeMenu()
        #invoking options in playMode button
        player.playModeMb.menu.invoke(1)
        self.assertEqual(player.playMode,0)
        player.playModeMb.menu.invoke(2)
        self.assertEqual(player.playMode,1)
        player.playModeMb.menu.invoke(3)
        self.assertEqual(player.playMode,2)
        player.playModeMb.menu.invoke(4)
        self.assertEqual(player.playMode,3)
        player.cancelPlayMode_cb()
        player.Close_cb()
        
    def test_confplayer_play_parameters(self):
        """tests confplayer play parameters:
        """
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        self.mv.ADanalyze_showStates("ind_out")
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        self.mv.Mols[0].spw = player
        #calling setplay options widget
        player.SetMode_cb()
        player.showFrameParms_cb()
        player.showFrameParmWidgets.set(1)
        #setting options in play parameters button
        player.framerateWidget.set(10)
        player.startFrameWidget.set(2)
        player.endFrameWidget.set(8)
        player.stepSizeWidget.set(2)
        player.GoToStart_cb()
        self.assertEqual(player.currentFrameIndex,2)
        player.GoToEnd_cb()
        self.assertEqual(player.currentFrameIndex,8)
        self.assertEqual(player.stepSize,2)
        self.assertEqual(player.framerate,10)
        player.framerateWidget.set(15)
        player.startFrameWidget.set(0)
        player.endFrameWidget.set(10)
        player.stepSizeWidget.set(1) 
        player.cancelPlayMode_cb()
        player.Close_cb()
        
    def test_confplayer_buildcurrent(self):
        """tests confpalyer build current:
        """
        #reading docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        self.mv.ADanalyze_showStates("ind_out")
        #calling player
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        player.SetMode_cb()
        player.nextFrame(1)
        #testing buildcurrent button
        player.Build_cb()
        self.assertEqual(len(self.mv.Mols.name),2)
        self.assertEqual(self.mv.Mols.name,['ind_out', 'ind_out_conf_1'])
        player.cancelPlayMode_cb()
        player.Close_cb() 
    
        
    def test_confplayer_buildall(self):
        """tests confplayer build all:
        """
        #reading docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByMolecules("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        self.mv.ADanalyze_showStates("ind_out")
        #calling player
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        player.SetMode_cb()
        #testing buildall button
        player.BuildAll_cb()
        self.assertEqual(len(self.mv.Mols.name),11)
        player.cancelPlayMode_cb()
        player.Close_cb()
        while len(self.mv.Mols):
            self.mv.deleteMol(self.mv.Mols[-1]) 

    def test_confplayer_colortype(self):
        """tests confpalyer color by button
        """
        #reading docking log
        self.mv.ADanalyze_readDLG('./ind.dlg', 0, 1, log=0)
        self.mv.colorByAtomType("ind_out", ('lines',), log=0)
        self.mv.setUserPreference(('Player GUI', 'ConformationPlayer'), log=0)
        self.mv.ADanalyze_showStates("ind_out")
        #calling player
        player =ConformationPlayer(mol =mv.Mols[0],docking = mv.dockings['ind.dlg'],vf  = mv,sequenceList = mv.docked.ch.conformations)
        oldcolor = mv.allAtoms.colors['lines']
        #testing color by button setting entry as molecule
        player.SetMode_cb()
        player.playModeForm.ent3.delete(0,'end')
        player.playModeForm.ent3.insert(0,'molecule')
        player.updateColor()
        newcolor = mv.allAtoms.colors['lines']
        self.assertEqual(oldcolor!=newcolor,True)





if __name__ == '__main__':
    unittest.main()
    
