#$Header: /opt/cvs/python/packages/share1.5/AutoDockTools/Tests/test_loadCommands.py,v 1.2 2010/05/26 21:45:39 annao Exp $
# Author: Sargis Dallakyan - sargis@scripps.edu
# Summary: Makes sure that all Pmv modules can be loaded.
#$Id: test_loadCommands.py,v 1.2 2010/05/26 21:45:39 annao Exp $
import unittest, sys, os, time
from mglutil.util.packageFilePath import findModulesInPackage
import AutoDockTools

class LoadTest(unittest.TestCase):
    """Base class for unittest"""
    def test_loadAllModules(self):
        from Pmv.moleculeViewer import MoleculeViewer
        mv = MoleculeViewer(customizer = './.empty', logMode = 'no',
                            withShell=0, trapExceptions=False)
        mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        packagePath = AutoDockTools.__path__[0]
        modules = findModulesInPackage(packagePath, "^def initModule")[packagePath]
        failedTest = []
        for module_py in modules:
            module = os.path.splitext(module_py)[0]
            print 'loading...' + module
            try:
                mv.loadModule(module, 'AutoDockTools')
            except:
                failedTest.append(module_py)
            
        self.assertEqual(failedTest,[])
        mv.Exit(0)
if __name__ == '__main__':
        unittest.main()

    
