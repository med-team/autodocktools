#
#
#  $Id: test_AutoLigandCommand.py,v 1.4 2011/06/13 20:04:05 annao Exp $
#
#

import unittest, glob, os, sys
from string import strip, find, split
import time
from MolKit.molecule import Atom

mv = None
ct = 0
totalCt = 1


class AutoLigandCommand_BaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    
        
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(trapExceptions=False)
            mv.loadModule('AutoLigandCommand', 'AutoDockTools')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)


    def tearDown(self):
        """
        clean-up
        """
        #print 'in flex tearDown'
        global ct, totalCt
        #reset flexDict object
        self.mv.flexDict = {}
        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        #print 'ct =', ct
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv       
    
        
from mglutil.gui.InputForm.Tk.gui import InputFormDescr
class AutoLigandCommandTests(AutoLigandCommand_BaseTests):
    
    
    def test_gui(self):
        """test_gui: check that the GUI is working
        """
        self.mv.GUI.ROOT.after(2000, self.envoke)
        self.mv.AutoLigandCommand.guiCallback()
        ans = 'l'
        while ans != 'normal':
            ans = self.mv.AutoLigandCommand.cmd.ok.configure()['state'][-1]
            self.mv.GUI.ROOT.update()

        
    def envoke(self):
        self.mv.AutoLigandCommand.ifd.entryByName['FileBaseName']['widget'].selectitem('AG4_hsg1')
        self.mv.AutoLigandCommand.selectGrid('AG4_hsg1')
        self.mv.AutoLigandCommand.ifd.form.OK_cb()
        #self.mv.GUI.ROOT.update()
        #self.mv.AutoLigandCommand.ifd.form.root.destroy()
#        self.mv.GUI.ROOT.after(10, self.check)

        
    def check(self):
        self.mv.GUI.ROOT.update()
            
if __name__ == '__main__':
    unittest.main()
