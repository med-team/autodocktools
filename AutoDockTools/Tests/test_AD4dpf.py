#
#
#  $Id: test_AD4dpf.py,v 1.13.2.1 2015/08/26 22:45:31 sanner Exp $
#
#


import unittest, glob, os
import time
from string import split, find, strip

mv = None
ct = 0
totalCt = 38
vina_keywords = ['receptor','flexres','flex','ligand','center_x','center_y','center_z','out','log','cpu','seed','exhaustiveness','num_modes','energy_range', 'config']


class ADdpf4_BaseTests(unittest.TestCase):
    """
    setUp + tearDown form a fixture: working environment for the testing code
    """
    
        
    def startViewer(self):
        global mv
        #print 'in test_AD startViewer'
        if mv is None:
            from MolKit import Read
            import Tkinter
            from Pmv.moleculeViewer import MoleculeViewer
            mv = MoleculeViewer(trapExceptions=False)
            mv.loadModule('autotorsCommands', 'AutoDockTools')
            mv.loadModule('autogpfCommands', 'AutoDockTools')
            mv.loadModule('autodpfCommands', 'AutoDockTools')
            #8/18:
            mv.loadModule('autoanalyzeCommands', 'AutoDockTools')
            mv.loadModule('deleteCommands', 'Pmv')
            mv.loadModule('selectionCommands', 'Pmv')
            #change warningMsg format
            mv.setUserPreference(('warningMsgFormat', 'printed'), log = 0)
        self.mv = mv


    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()


    def tearDown(self):
        """
        clean-up
        """
        #print 'in gpf tearDown'
        global ct, totalCt
        #reset docked
        from AutoDockTools.DockingParameters import DockingParameters
        self.mv.dpo = DockingParameters()
        self.mv.dpo.vf = self.mv
        from AutoDockTools.GridParameters import GridParameters
        self.mv.gpo = GridParameters()
        self.mv.gpo.vf=self.mv
        self.mv.DPF_LIGAND_TYPES = []
        self.mv.DPF_FLEXRES_TYPES = []

        #delete any molecules left due to errors
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        ct = ct + 1
        #print 'ct =', ct
        if ct==totalCt:
            print 'destroying mv'
            self.mv.Exit(0)
            del self.mv            
    

    def SA_GA_LS_GALS(self):    
        self.mv.ADdpf4_writeSA("ref4_SA.dpf")
        self.mv.ADdpf4_writeGA("ref4_GA.dpf")
        self.mv.ADdpf4_writeLS("ref4_LS.dpf")
        self.mv.ADdpf4_writeGAlS("ref4_GALS.dpf")



class ADdpf4_readDPFTests(ADdpf4_BaseTests):
    
    
    def test_dpf_readdpf(self):
        """reads a dpf and checks
        """ 
        filename = 'ref_1ebg.dpf'
        self.mv.ADdpf_read(filename)
        c=self.mv.dpo
        self.assertEqual(c.dpf_filename, filename)


    def test_dpf_read_empty_dpf(self):
        """checks empty dpf entry
        """
        self.assertRaises(IOError, self.mv.ADdpf_read, " ")


    def test_dpf_read_non_existent_dpf(self):
        """checks non-existent dpf
        """
        self.assertRaises(IOError, self.mv.ADdpf_read, "asd.hfd.dpf")
    
   
    def test_dpf_set_macro_molecule_file_name(self):
        """checks  macro mols filename
        """
        filename = "1ebg_rec.pdbqt"
        self.mv.ADdpf4_readMacromolecule(filename)
        self.assertEqual(self.mv.dpo.receptor_filename, filename)
             

    def test_dpf_read_empty_macro_mol(self):
   
        """checks empty macro mol entry
        """
        self.assertRaises(IOError, self.mv.ADdpf4_readMacromolecule, " ")


    def test_dpf_read_non_existent_macro_mol(self):
        """checks non-existent macro mol        """
        self.assertRaises(IOError, self.mv.ADdpf4_readMacromolecule, "asd.hfd.dpf")
    

    def test_dpf_replace_random(self):
        """ checks by setting some values in place of random
        """
        self.mv.ADdpf_read('ref_1ebg.dpf')
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        teststring = "1.0 2.0 3.0"
        self.mv.ADdpf_setDpo(tran0=teststring)
        testfile = "whateveryouwant.dpf"
        self.mv.ADdpf4_writeGALS(testfile)
        fptr=open(testfile)
        alllines=fptr.readlines()
        for l in alllines:
            if find(l, 'tran0')==0:
                break
        ll = split(l)
        testsplit = split(teststring)
        for i in range(1, 4):
            self.assertEqual(ll[i], testsplit[i-1])
   

    def test_chosen_ligand_name(self):
        
        """checks chosen ligand name
        """
        self.mv.readMolecule('1ebg_lig.pdbqt')
        lig_name = self.mv.Mols[0].name
        self.mv.ADdpf4_chooseFormattedLigand(lig_name)
        self.assertEqual(self.mv.dpo.ligand.name, lig_name)
        

    def test_dpf_set_ligand_parameters(self):
        """ checks ligand name, ndihe, center, types
        """
        filename = '1ebg_lig.pdbqt'
        self.mv.readMolecule(filename)
        m=self.mv.Mols[0]
        self.mv.ADdpf4_chooseFormattedLigand(m.name)
        #4assertions: move is ligand.parser.filename
        #ndihe == ligand.ndihe
        #types == ligand.types
        #about==ligand.center
        center = m.getCenter()
        cen2 = []
        for i in range(3):
            cen2.append(round(center[i], 3))
        self.assertEqual(m.ndihe, self.mv.dpo['ndihe']['value'])
        self.assertEqual(m.types, self.mv.dpo['types']['value'])
        self.assertEqual(cen2, self.mv.dpo['about']['value'])
        #self.assertEqual(m.center, self.mv.dpo['about']['value'])
        self.assertEqual(filename, self.mv.dpo.ligand_filename)
   

    def test_dpf_read_empty_ligand(self):
        """checks empty ligand
        """
        self.assertRaises(IOError, self.mv.ADdpf4_readFormattedLigand, " ")


    def test_dpf_read_non_existent_ligand(self):
        """checks non-existent ligand
        """
        self.assertRaises(IOError, self.mv.ADdpf4_readFormattedLigand, "asd.hfd.dpf")

    
    def test_dpf_read_ligand_widget_exists(self):
        """checks for widget whem read ligand called
        """
        self.mv.ADdpf4_readFormattedLigand('1ebg_lig.pdbqt')
        c = self.mv.ADdpf4_initLigand
        self.assertEqual(c.form.root.winfo_ismapped(), 1)
        self.assertEqual(c.form.root.withdraw(),'')

     
    def test_search_parameters_simulated_annealing_replace(self):
        """checks after replacing
        """
        self.mv.ADdpf_setDpo(rtrf='0.5')
        self.assertEqual(self.mv.dpo['rtrf']['value'],'0.5')
        

    def test_search_parameters_ga_parameters_change(self):
        """checks after replacing
        """
        d={}
        d['ga_mutation_rate'] =0.5 
        apply(mv.ADdpf_setGAparameters, (), d)
        self.assertEqual(self.mv.dpo['ga_mutation_rate']['value'],0.5)
               
    
    def test_search_parameters_ga_parameters_values(self):
        """checks values in widget and file are same
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        testfile = "somename.dpf"
        self.mv.ADdpf4_writeGALS(testfile)
        fptr = open(testfile)
        alllines = fptr.readlines()
        fptr.close()
        ll = [0, 0]
        for i in alllines:        
            if find(i,'ga_window_size')==0:
                ll=split(i)
                break
        self.assertEqual(str(mv.dpo['ga_window_size']['value']), ll[1])
    

    def test_search_parameters_ga_parameters_replace(self):
        """checks in file after replacing in widget
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        d={}
        d['ga_pop_size'] = 2
        apply(self.mv.ADdpf_setGAparameters, (), d)
        testfile = "somename.dpf"
        self.mv.ADdpf4_writeGALS(testfile)
        fptr=open(testfile)
        alllines=fptr.readlines()
        fptr.close()
        ll = [0, 0]
        for i in alllines:        
            if find(i,'ga_pop_size')==0:
                ll=split(i)
                break
        self.assertEqual(ll[1],'2')
        
   
    def test_search_parameters_ls_parameters_values(self):
        """checks after replacing ls parameter
        """
        d={}
        d['ls_search_freq'] =0.5 
        apply(mv.ADdpf_setLSparameters, (), d)
        self.assertEqual(self.mv.dpo['ls_search_freq']['value'],0.5)
   

    def test_search_parameters_ls_parameters_replace(self):
        """checks ls  parameters in widget and file are same
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        d={}
        d['ls_search_freq'] = 0.02 
        apply(mv.ADdpf_setLSparameters, (), d)
        testfile = "somename.dpf"
        self.mv.ADdpf4_writeGALS(testfile)
        fptr=open(testfile)
        alllines=fptr.readlines()
        ll = [0, 0]
        for i in alllines:        
            if find(i,'ls_search_freq')==0:
                ll=split(i)
                break
        self.assertEqual(str(round(mv.dpo['ls_search_freq']['value'],2)),ll[1])
        

    def test_search_parameters_ls_parameters_change(self):
        """checks in file after replacing ls parameters in widget
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        d={}
        d['ls_search_freq'] = 0.02
        apply(mv.ADdpf_setLSparameters, (), d)
        testfile = "somename.dpf"
        self.mv.ADdpf4_writeGALS(testfile)
        fptr=open(testfile)
        alllines=fptr.readlines()
        fptr.close()
        ll = [0, 0]
        for i in alllines:        
            if find(i,'ls_search_freq')==0:
                ll=split(i)
                break
        self.assertEqual(ll[1],'0.02')
   

    def test_search_parameters_docking_parameters_values(self):
        """checks docking parameters after replacing
        """
        d={}
        d['rmstol'] = 4.0 
        apply(mv.ADdpf_setDockingParameters, (), d)
        self.assertEqual(self.mv.dpo['rmstol']['value'], 4.0)


    def test_search_parameters_docking_parameters_replace(self):
        """checks docking  paarmeters in widget and file are same
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        test_val = 123
        d={}
        d['sw_max_fail'] = test_val
        apply(mv.ADdpf_setDockingParameters, (), d)
        testfile = "somename.dpf"
        self.mv.ADdpf4_writeGALS(testfile)
        fptr=open(testfile)
        alllines=fptr.readlines()
        fptr.close()
        ll = [0,0]
        for line in alllines:        
            if find(line,'sw_max_fail')>-1:
                ll=split(line)
                break
        self.assertEqual(test_val,int(ll[1]))
        self.assertEqual(self.mv.dpo['sw_max_fail']['value'],test_val)
         

    def test_search_parameters_docking_parameters_change(self):
        """checks in file after replacing ls parameters in widget
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        test_val = 321
        d={}
        d['sw_max_its'] =  test_val
        apply(mv.ADdpf_setDockingParameters, (), d)
        testfile = "somename.dpf"
        self.mv.ADdpf4_writeGALS(testfile)
        fptr=open(testfile)
        alllines=fptr.readlines()
        fptr.close()
        ll = [0,0]
        for i in alllines:        
            if find(i,'sw_max_its')==0:
                ll=split(i)
                break
        self.assertEqual(int(ll[1]),test_val)


    def test_sa_dpf_compare_lines(self):
        
        """checks ref4_SA file and test_SA file are same
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        self.mv.ADdpf4_writeSA("test_SA.dpf")
        testptr = open("test_SA.dpf")
        testlines = testptr.readlines()
        refptr = open("ref4_SA.dpf")
        reflines = refptr.readlines()
        refptr.close()
        for refline,testline in zip(reflines,testlines):
            self.assertEqual(refline,testline)
            

    def test_ga_dpf_compare_lines(self):
        
        """checks ref4_GA file and test_GA file are same
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        self.mv.ADdpf4_writeGA("test_GA.dpf")
        testptr = open("test_GA.dpf")
        testlines = testptr.readlines()
        refptr = open("ref4_GA.dpf")
        reflines = refptr.readlines()
        refptr.close()
        for refline,testline in zip(reflines,testlines):
            self.assertEqual(refline,testline)
            #ref_types = set(refline.split()[1:6] )
            #test_types = set(testline.split()[1:6] )
            #self.assertEqual(ref_types,test_types)

    def test_ls_dpf_compare_lines(self):
        
        """checks ref4_LS file and test_LS file are same
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        self.mv.ADdpf4_writeLS("test_LS.dpf")
        testptr = open("test_LS.dpf")
        testlines = testptr.readlines()
        refptr = open("ref4_LS.dpf")
        reflines = refptr.readlines()
        refptr.close()
        for refline,testline in zip(reflines,testlines):
            if refline.find("ligand_types")>-1:
                ref_words = refline.split()
                test_words = testline.split()
                self.assertEqual(len(ref_words),len(test_words))
                for w in ref_words:
                    assert w in test_words
            else:
                self.assertEqual(strip(refline), strip(testline))


    def test_gals_dpf_compare_lines(self):
        
        """checks ref4_GALS file and test_GALS file are same
        """
        self.mv.ADdpf4_readFormattedLigand("1ebg_lig.pdbqt")
        self.mv.ADdpf4_readMacromolecule("1ebg_rec.pdbqt")
        self.mv.ADdpf4_writeGALS("test_GALS.dpf")
        testptr = open("test_GALS.dpf")
        testlines = testptr.readlines()
        refptr = open("ref4_GALS.dpf")
        reflines = refptr.readlines()
        refptr.close()
        for refline,testline in zip(reflines,testlines):
            if refline.find("ligand_types")>-1:
                ref_words = refline.split()
                test_words = testline.split()
                self.assertEqual(len(ref_words),len(test_words))
                for w in ref_words:
                    assert w in test_words
            else:
                self.assertEqual(strip(refline), strip(testline))


class ADdpf4_flexResTests(ADdpf4_BaseTests):

    #read receptor
    def test_dpf4_select_rigid_macro_mol(self):
        """select a rigid receptor and checks dpo receptor_filename 
        """
        filename = 'hsg1_rigid.pdbqt'
        self.mv.ADdpf4_readMacromolecule(filename)
        self.assertEqual(self.mv.dpo.receptor_filename,filename)


    #read ligand
    def test_dpf4_read_flex_mol(self):
        """reads flexible input file and checks dpo ligand_filename 
        """
        filename = 'hsg1_flex.pdbqt'
        self.mv.ADdpf4_readFormattedLigand(filename)
        ligand_name = self.mv.Mols[0].name
        self.assertEqual(self.mv.dpo.ligand.name, ligand_name)
        self.assertEqual('ARG8' in self.mv.Mols[0].chains.residues.name, True)
        args = self.mv.Mols[0].chains.residues.get(lambda x:x.name=='ARG8')
        self.assertEqual(len(args), 2)
        self.assertEqual(len(args.atoms.bonds[0])>0, True)
    

    #choose ligand
    def test_dpf4_choose_flex_mol(self):
        """readin then choose flexible input file. check dpo ligand_filename 
        """
        filename = 'hsg1_flex.pdbqt'
        self.mv.readMolecule(filename)
        ligand_name = self.mv.Mols[0].name
        self.assertEqual('ARG8' in self.mv.Mols[0].chains.residues.name, True)
        args = self.mv.Mols[0].chains.residues.get(lambda x:x.name=='ARG8')
        self.assertEqual(len(args), 2)
        self.assertEqual(len(args.atoms.bonds[0])>0, True)
        self.mv.ADdpf4_chooseFormattedLigand(ligand_name)
        self.assertEqual(self.mv.dpo.ligand.name, ligand_name)



class ADdpf4_formatting_flexResTests(ADdpf4_BaseTests):

    def setUp(self):
        """
        clean-up
        """
        if not hasattr(self, 'mv'):
            self.startViewer()
        for m in self.mv.Mols:
            self.mv.deleteMol(m)
        self.mv.loadModule('autoflexCommands', 'AutoDockTools')
        self.mv.readMolecule('hsg1.pdbqt', ask=0, parser=None)
        self.mv.ADflex_chooseMacro("hsg1")
        from MolKit.molecule import Atom
        self.mv.setIcomLevel(Atom, KlassSet=None)
        self.mv.ADflex_setResidues("::ARG8")
        self.mv.ADflex_setBondRotatableFlag("hsg1:B:ARG8:NE,CZ;", False)
        self.mv.ADflex_setBondRotatableFlag("hsg1:B:ARG8:CZ,NH1;", False)
        self.mv.ADflex_setBondRotatableFlag("hsg1:B:ARG8:CZ,NH2;", False)
        self.mv.ADflex_setBondRotatableFlag("hsg1:A:ARG8:NE,CZ;", False)
        self.mv.ADflex_setBondRotatableFlag("hsg1:A:ARG8:CZ,NH1;", False)
        self.mv.ADflex_setBondRotatableFlag("hsg1:A:ARG8:CZ,NH2;", False)
        self.mv.ADflex_setBondRotatableFlag("hsg1:B:ARG8:CA,CB;", False)
        self.mv.ADflex_setBondRotatableFlag("hsg1:A:ARG8:CA,CB;", False)
        self.mv.ADflex_writeFlexFile('temp_FLEX.pdbqt')
        self.mv.ADflex_writeRigidFile('temp_RIGID.pdbqt')


    #choose receptor
    def test_dpf4_choose_rigid_mol(self):
        """choose new rigid file. check dpo macro_filename 
        """
        written_macro_name = "temp_RIGID.pdbqt"
        self.mv.ADdpf4_readMacromolecule(written_macro_name)
        macro_name = "temp_RIGID"
        self.assertEqual(self.mv.dpo.receptor_stem, macro_name)
        self.assertEqual(self.mv.dpo['fld']['value'], macro_name+".maps.fld")



class ADdpf4_FlexRes_Lig_Order_Tests(ADdpf4_BaseTests):

    def test_gpf4_lig_flexres(self):
        """flexResTests: read a ligand file then a flexres file and check written types
        """
        rigid_filename = 'CathepsinB_rigid.pdbqt'
        flexres_filename = 'CathepsinB_flex.pdbqt'
        ligand_filename = 'FGA3.pdbqt'
        output_filename = 'lig_flexres.dpf'
        self.mv.ADdpf4_readMacromolecule(rigid_filename)
        self.mv.ADdpf4_readFormattedLigand(ligand_filename)
        self.mv.ADdpf4_readFlexRes(flexres_filename)
        self.mv.ADdpf4_writeGALS(output_filename, log=0)
        fptr = open(output_filename)
        lines = fptr.readlines()
        types_line = lines[4]
        ref_line = "ligand_types A C OA HD N SA          # atoms types in ligand\n"
        self.assertEqual(types_line,ref_line)


    def test_gpf4_flexres_lig(self):
        """flexResTests: read a flexres file then a ligand file and check written types
        """
        rigid_filename = 'CathepsinB_rigid.pdbqt'
        flexres_filename = 'CathepsinB_flex.pdbqt'
        ligand_filename = 'FGA3.pdbqt'
        output_filename = 'flexres_lig.dpf'
        self.mv.ADdpf4_readMacromolecule(rigid_filename)
        self.mv.ADdpf4_readFlexRes(flexres_filename)
        self.mv.ADdpf4_readFormattedLigand(ligand_filename)
        self.mv.ADdpf4_writeGALS(output_filename)
        fptr = open(output_filename)
        lines = fptr.readlines()
        types_line = lines[4]
        ref_line = "ligand_types A C OA HD N SA          # atoms types in ligand\n"
        self.assertEqual(types_line,ref_line)


    
    
    def test_rig_flex_lig(self):
        """set rigid filename, flexfilename and open ligand
        """ 
        self.mv.ADdpf4_readMacromolecule('./hsg1_rigid.pdbqt', log=0)
        self.mv.ADdpf4_readFlexRes('./hsg1_FE.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_FLEXRES_TYPES, ['FE'])
        self.mv.ADdpf4_readFormattedLigand('./ind_zero.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_LIGAND_TYPES, ['C'])
        self.assertEqual(self.mv.DPF_FLEXRES_TYPES, ['FE'])
        self.mv.ADdpf4_writeGALS('./zero_FE_rig_GALS.dpf', log=0)
        c=self.mv.dpo
        self.assertEqual(c['ligand_types']['value'], 'C FE')

    
    def test_rig_lig_flex(self):
        """set rigid filename, open ligand and set flexfilename
        """ 
        self.mv.ADdpf4_readMacromolecule('./hsg1_rigid.pdbqt', log=0)
        self.mv.ADdpf4_readFormattedLigand('./ind_zero.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_LIGAND_TYPES, ['C'])
        self.mv.ADdpf4_readFlexRes('./hsg1_FE.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_FLEXRES_TYPES, ['FE'])
        self.mv.ADdpf4_writeGALS('./zero_FE_rig_GALS.dpf', log=0)
        c=self.mv.dpo
        self.assertEqual(c['ligand_types']['value'], 'C FE')

    
    def test_flex_rig_lig(self):
        """set  flex filename, rigid filename and open ligand
        """ 
        self.mv.ADdpf4_readFlexRes('./hsg1_FE.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_FLEXRES_TYPES, ['FE'])
        self.mv.ADdpf4_readMacromolecule('./hsg1_rigid.pdbqt', log=0)
        self.mv.ADdpf4_readFormattedLigand('./ind_zero.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_LIGAND_TYPES, ['C'])
        c=self.mv.dpo
        self.mv.ADdpf4_writeGALS('./zero_rig_GALS.dpf', log=0)
        self.assertEqual(c['ligand_types']['value'], 'C FE')

    
    def test_flex_lig_rig(self):
        """set  flex filename, open ligand and set rigid filename
        """ 
        self.mv.ADdpf4_readFlexRes('./hsg1_FE.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_FLEXRES_TYPES, ['FE'])
        self.mv.ADdpf4_readFormattedLigand('./ind_zero.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_LIGAND_TYPES, ['C'])
        self.mv.ADdpf4_readMacromolecule('./hsg1_rigid.pdbqt', log=0)
        c=self.mv.dpo
        self.mv.ADdpf4_writeGALS('./zero_rig_GALS.dpf', log=0)
        self.assertEqual(c['ligand_types']['value'], 'C FE')

    
    def test_lig_rig_flex(self):
        """set open ligand, set rigid filename and set flex filename
        """ 
        self.mv.ADdpf4_readFormattedLigand('./ind_zero.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_LIGAND_TYPES, ['C'])
        self.mv.ADdpf4_readMacromolecule('./hsg1_rigid.pdbqt', log=0)
        self.mv.ADdpf4_readFlexRes('./hsg1_FE.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_FLEXRES_TYPES, ['FE'])
        self.mv.ADdpf4_writeGALS('./zero_rig_FE_GALS.dpf', log=0)
        c=self.mv.dpo
        self.assertEqual(c['ligand_types']['value'], 'C FE')

    
    def test_lig_flex_rig(self):
        """set open ligand, set flex filename and set rigid filename
        """ 
        self.mv.ADdpf4_readFormattedLigand('./ind_zero.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_LIGAND_TYPES, ['C'])
        self.mv.ADdpf4_readFlexRes('./hsg1_FE.pdbqt', log=0)
        self.assertEqual(self.mv.DPF_FLEXRES_TYPES, ['FE'])
        self.mv.ADdpf4_readMacromolecule('./hsg1_rigid.pdbqt', log=0)
        self.mv.ADdpf4_writeGALS('./zero_FE_rig_GALS.dpf', log=0)
        c=self.mv.dpo
        self.assertEqual(c['ligand_types']['value'], 'C FE')



if __name__ == '__main__':
    #unittest.main()
    test_cases = [
        'ADdpf4_BaseTests',
        'ADdpf4_readDPFTests',
        'ADdpf4_flexResTests',
        'ADdpf4_formatting_flexResTests',
        'ADdpf4_FlexRes_Lig_Order_Tests',
            ]
    unittest.main( argv=([__name__ ,] + test_cases) )



