#
#
#
#
# $Header: /opt/cvs/python/packages/share1.5/AutoDockTools/Tests/test_dlg_filters.py,v 1.4 2010/05/10 23:53:21 annao Exp $
#
# $Id: test_dlg_filters.py,v 1.4 2010/05/10 23:53:21 annao Exp $
#
#
#
# 
#

import unittest, os, sys
from AutoDockTools.DlgFilters import Filter, EnergyFilter, ClusterSizeFilter, ClusterPercentageFilter,\
                                     EnergyClusterSizeFilter, BestEnergyInLargestCluster, \
                                     EnergyPlusBestEnergyInLargestCluster, LigandEfficiencyFilter, \
                                     InteractionFilter


class FilterBaseTests(unittest.TestCase):
    def setUp(self):
        """
        instantiate an EnergyFilter
        """
        filter = Filter()
        self.assertEquals(filter.__class__, Filter)

        
class EnergyFilterTests(FilterBaseTests):

    def test_PassingEnergyFilter(self):
        """
        lowest energy in dlg -11.14 passes compared to -11 
        """
        self.ef = EnergyFilter(-11)
        self.assertEqual(self.ef.filter("ind4.1.1.DLG"), True)

    def test_PassingEnergyFilter2(self):
        """
        lowest energy in dlg -11.14 passes compared to -11.1 
        """
        self.ef = EnergyFilter(-11)
        self.assertEqual(self.ef.filter("ind4.1.1.DLG"), True)


    def test_FailingEnergyFilter(self):
        """
        lowest energy in dlg -11.14 fails compared to -12 
        """
        self.ef = EnergyFilter(-12)
        self.assertEqual(self.ef.filter("ind4.1.1.DLG"), False)


        
class ClusterSizeFilterTests(FilterBaseTests):

    def test_PassingClusterSizeFilter(self):
        """
        largest cluster size in dlg 8 passes compared to 6
        """
        self.csf = ClusterSizeFilter(cluster_size=6)
        self.assertEqual(self.csf.filter("ind4.1.1.DLG"), True)


    def test_PassingClusterSizeFilter0(self):
        """
        largest cluster size in dlg 8 passes compared to 0
        """
        self.csf = ClusterSizeFilter(cluster_size=0)
        self.assertEqual(self.csf.filter("ind4.1.1.DLG"), True)


    def test_FailingClusterSizeFilter(self):
        """
        largest cluster size in dlg 8 fails compared to 9
        """
        self.csf = ClusterSizeFilter(cluster_size=9)
        self.assertEqual(self.csf.filter("ind4.1.1.DLG"), False)


        
class ClusterPercentageFilterTests(FilterBaseTests):

    def test_PassingClusterPercentageFilterTests(self):
        """
        percentage in cluster in dlg 80 passes compared to 60
        """
        self.csf = ClusterPercentageFilter(percentage=60)
        self.assertEqual(self.csf.filter("ind4.1.1.DLG"), True)


    def test_PassingClusterPercentageFilterTests2(self):
        """
        percentage in cluster in dlg 80 passes compared to 80
        """
        self.csf = ClusterPercentageFilter(percentage=80)
        self.assertEqual(self.csf.filter("ind4.1.1.DLG"), True)


    def test_FailingClusterPercentageFilterTests(self):
        """
        percentage in cluster in dlg 80 fails compared to 90
        """
        self.csf = ClusterPercentageFilter(percentage=90)
        self.assertEqual(self.csf.filter("ind4.1.1.DLG"), False)


class EnergyClusterSizeFilterTests(FilterBaseTests):

    def test_PassingEnergyClusterSizeFilter(self):
        """
        energy -11 and cluster_size 6 pass compared to -11.14 and 8 in dlg 
        """
        self.ecsf = EnergyClusterSizeFilter(energy=-11, cluster_size=6)
        self.assertEqual(self.ecsf.filter("ind4.1.1.DLG"), True)


    def test_FailingEnergyClusterSizeFilter(self):
        """
        energy -11 and cluster_size 9 fail compared to -11.14 and 8 in dlg 
        """
        self.ecsf = EnergyClusterSizeFilter(energy=-11, cluster_size=9)
        self.assertEqual(self.ecsf.filter("ind4.1.1.DLG"), False)


    def test_FailingEnergyClusterSizeFilter2(self):
        """
        energy -12 and cluster_size 6 fail compared to -11.14 and 8 in dlg 
        """
        self.ecsf = EnergyClusterSizeFilter(energy=-12, cluster_size=6)
        self.assertEqual(self.ecsf.filter("ind4.1.1.DLG"), False)



class BestEnergyInLargestClusterTests(FilterBaseTests):

    def test_PassingBestEnergyInLargestCluster(self):
        """
        cluster containing best energy conformation is also the largest cluster 
        """
        self.ecsf = BestEnergyInLargestCluster()
        self.assertEqual(self.ecsf.filter("ind4.1.1.DLG"), True)


    def test_FailingBestEnergyInLargestCluster(self):
        """
        cluster containing best energy conformation is NOT also the largest cluster 
        """
        self.ecsf = BestEnergyInLargestCluster()
        self.assertEqual(self.ecsf.filter("0033.dlg"), False)



class LigandEfficiencyFilterTests(FilterBaseTests):

    def test_PassingLigandEfficiencyFilter(self):
        """
        LigandEfficiency is the best overall energy/number of atoms in the ligand 
        """
        self.ecsf = LigandEfficiencyFilter(-0.2) 
        self.assertEqual(self.ecsf.filter("ind4.1.1.DLG"), True)


    def test_FailingLigandEfficiencyFilter(self):
        """
        cluster containing best energy conformation is NOT also the largest cluster 
        """
        self.ecsf2 = LigandEfficiencyFilter(-0.25)
        #-0.22918367 when 4 hydrogens were included in the count , -0.2476 without them
        self.assertEqual(self.ecsf2.filter("ind4.1.1.DLG"), False)


class InteractionFilterTests(FilterBaseTests):

    def test_PassingInteractionFilter(self):
        """
        two atoms interact if the distance between them is less than the sum
        of their vdw radii
        """
        self.intF = InteractionFilter("hsg1.pdbqt")
        self.assertEqual(self.intF.filter("ind4.1.1.DLG"), 34)


    def test_FailingInteractionFilter(self):
        """
        no interactions between docked ligand and hsg1.pdbqt
        """
        self.intF = InteractionFilter("1dwd_rec.pdbqt")
        self.assertEqual(self.intF.filter("ind4.1.1.DLG"), 0)




if __name__ == '__main__':
    #unittest.main()
    test_cases = [
        'EnergyFilterTests',
        'ClusterSizeFilterTests',
        'ClusterPercentageFilterTests',
        'EnergyClusterSizeFilterTests',
        'BestEnergyInLargestClusterTests',
        'LigandEfficiencyFilterTests',
        'InteractionFilterTests',
            ]
    unittest.main( argv=([__name__ ,] + test_cases) )


    
