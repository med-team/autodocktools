#
#
#
#
# $Id: test_AutoDockBondClassifier.py,v 1.1 2004/06/08 18:56:41 rhuey Exp $
#

import unittest, os
from string import split
from AutoDockTools.AutoDockBondClassifier import AutoDockBondClassifier
from MolKit import molecule, protein
molecule.bhtreeFlag = 0
protein.bhtreeFlag = 0


class BaseTests(unittest.TestCase):
    def setUp(self):
        from MolKit import Read
        self.mol = Read('ind.out.pdbq')[0]
        self.mol.buildBondsByDistance()
    

    def tearDown(self):
        """
        clean-up
        """
        del(self.mol)


    def test_constructor(self):
        """
        instantiate an AutoDockBondClassifier
        """
        adbc = AutoDockBondClassifier()
        self.assertEquals(adbc.__class__, AutoDockBondClassifier)



    def test_classify(self):
        """
         test that AutoDockBondClassifier can classify bonds
        """
        adbc = AutoDockBondClassifier()
        resultDict = adbc.classify(self.mol.allAtoms.bonds[0])
        for k in ['amide', 'ppbb','leaf','cycle','rotatable',
                    'hydrogenRotators', 'guanidinium']:
            self.assertEquals(resultDict.has_key(k), True)


    def test_classify_results(self):
        """
         test results set by adbc
        """
        adbc = AutoDockBondClassifier()
        resultDict = adbc.classify(self.mol.allAtoms.bonds[0])
        results = {}
        results['amide'] = 2
        results['ppbb'] = 0
        results['leaf'] = 9
        results['cycle'] = 28
        results['rotatable'] = 16
        results['hydrogenRotators'] = 2
        results['guanidinium'] = 0
        for k, v in results.items():
            #print k, ':', v,' vs ', len(resultDict[k])
            self.assertEquals(len(resultDict[k]), v)



if __name__ == '__main__':
    unittest.main()
