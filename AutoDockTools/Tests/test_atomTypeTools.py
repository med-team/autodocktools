#
#
#
#
# $Id: test_atomTypeTools.py,v 1.1 2004/06/08 19:18:45 rhuey Exp $
#

import unittest, os
from string import split
from AutoDockTools.atomTypeTools import SolvationParameterizer
from MolKit import molecule, protein
molecule.bhtreeFlag = 0
protein.bhtreeFlag = 0
from MolKit import Read
from MolKit.pdbParser import PdbParser

from MolKit.chargeCalculator import GasteigerChargeCalculator, KollmanChargeCalculator


class SolvationParameterizerBaseTests(unittest.TestCase):
    def setUp(self):
        #read it as a pdb file to remove charges + solpars
        mols = PdbParser('hsg1.pdbqs').parse()
        self.mol = mols[0]
        self.mol.buildBondsByDistance()

        kollCC = KollmanChargeCalculator()
        kollCC.addCharges(self.mol.allAtoms)
    

    def tearDown(self):
        """
        clean-up
        """
        del(self.mol)


    def test_constructor(self):
        """
        instantiate an SolvationParameterizer
        """
        solpar = SolvationParameterizer()
        self.assertEquals(solpar.__class__, SolvationParameterizer)



    def test_addParameters(self):
        """
         test that SolvationParameterizer can addParameters
        """
        solpar = SolvationParameterizer()
        notfound = solpar.addParameters(self.mol.allAtoms)
        self.assertEquals(len(notfound), 0)


    def test_addParameters_vs_addsol(self):
        """
         test that SolvationParameterizer correctly handles charges
        """
        solpar = SolvationParameterizer()
        notfound = solpar.addParameters(self.mol.allAtoms)
        mol2 = Read("hsg1.pdbqs")[0]
        self.mol.allAtoms.sort()
        mol2.allAtoms.sort()
        for a1,a2 in zip(self.mol.allAtoms, mol2.allAtoms):
            self.assertEquals(a1.AtVol, a2.AtVol)
            self.assertEquals(a1.AtSolPar, a2.AtSolPar)



if __name__ == '__main__':
    unittest.main()
