#
#
#
#
# $Header: /opt/cvs/python/packages/share1.5/AutoDockTools/Tests/test_InteractionDetector.py,v 1.3 2010/06/14 23:49:18 rhuey Exp $
#
# $Id: test_InteractionDetector.py,v 1.3 2010/06/14 23:49:18 rhuey Exp $
#
#
#
# 
#

import unittest, os, sys
from AutoDockTools.InteractionDetector import InteractionDetector


class InteractionDetectorBaseTests(unittest.TestCase):
    def setUp(self):
        """
        instantiate an InteractionDetector
        """
        intD = InteractionDetector("hsg1V.pdbqt")
        self.assertEquals(intD.__class__, InteractionDetector)

        
class InteractionDetectorTests(InteractionDetectorBaseTests):

    def test_InteractionDetector(self):
        """
        check for number of hb atoms... etc 
        NOTE the atoms are found in random order so here we test for presence
        of various named atoms in the entire set of results...
        """
        self.intD = InteractionDetector("hsg1V.pdbqt")
        resStr = self.intD.processLigand("clean.pdbqt", outputfilename='filtered_clean.pdbqt')
        resStr_list = resStr.split('\n')
        all_strs = [ 'USER  AD> lig_hb_atoms : 5', 'USER  AD> clean: : INI 20:N4,H3~hsg1V:B:GLY27:O' , 'USER  AD> clean: : INI 20:O4,H3~hsg1V:B:ASP29:OD2' , 'USER  AD> hsg1V:B:ASP29:N,HN~clean: : INI 20:O4', 'USER  AD> clean: : INI 20:N4,H3~hsg1V:B:GLY27:O' , 'USER  AD> hsg1V:B:ASP29:N,HN~clean: : INI 20:O4', 'USER  AD> clean: : INI 20:O2,H2~hsg1V:B:ASP25:OD2', 'USER  AD> hsg1V:B:ARG8:NH2,HH22~clean: : INI 20:N5', 'USER  AD> macro_close_ats: 25', 'USER  AD> lig_close_ats: 18', 'USER  AD> USER clean: : INI 20:C2,C2,C1,C3,O4,C9,C2,C2,N5,H2,C1,C8,H3,C2,C3,H3,O2,C1']
        all_strs.append("USER  AD> clean: : INI 20:O2,H2~hsg1V:B:ASP25:OD1")
        all_strs.append("USER  AD> hsg1V:B:ARG8:NH2,HH22~clean: : INI 20:N5")
        self.assertEqual(resStr_list[0] in all_strs, True)
        self.assertEqual(resStr_list[1] in all_strs, True)
        self.assertEqual(resStr_list[2] in all_strs, True)
        self.assertEqual(resStr_list[3] in all_strs, True)
        self.assertEqual(resStr_list[4] in all_strs, True)
        self.assertEqual(resStr_list[5] in all_strs, True)
        self.assertEqual(resStr_list[6] in all_strs, True)
        self.assertEqual(resStr_list[8] in all_strs, True)
        #\nclean: : INI 20:N4,H3~hsg1V:B:GLY27:O\nclean: : INI 20:O4,H3~hsg1V:B:ASP29:OD2\nhsg1V:B:ASP29:N,HN~clean: : INI 20:O4\nclean: : INI 20:O2,H2~hsg1V:B:ASP25:OD1\nhsg1V:B:ARG8:NH2,HH22~clean: : INI 20:N5\nmacro_close_ats: 25\nUSER hsg1V:B:ARG8:HH22;hsg1V:A:ARG8:HH22;hsg1V:B:VAL32:CG2;hsg1V:B:GLY27:O;hsg1V:A:ASP25:CG;hsg1V:B:ASP30:HN;hsg1V:A:VAL82:CG1;hsg1V:A:ARG8:NH2;hsg1V:B:ASP25:CG;hsg1V:B:ASP29:HN;hsg1V:B:ALA28:CB;hsg1V:A:ARG8:CZ;hsg1V:B:ASP30:OD2;hsg1V:B:VAL82:CG2;hsg1V:B:ASP25:OD1;hsg1V:B:ASP30:CB;hsg1V:B:ASP25:OD2;hsg1V:A:ASP25:OD1;hsg1V:B:ASP29:OD2;hsg1V:B:ASP30:O;hsg1V:A:ARG8:HH21;hsg1V:B:ASP29:CG;hsg1V:B:PRO81:CG;hsg1V:B:PRO81:CB;hsg1V:B:ILE84:CD1\nlig_close_ats: 18\nUSER clean: : INI 20:C2,C2,C1,C3,O4,C9,C2,C2,N5,H2,C1,C8,H3,C2,C3,H3,O2,C1\n")
        


    def test_InteractionDetector_with_detect_pi(self):
        """
        check  detections of pi interactions as well as for number of hb atoms... etc 
        NOTE the atoms are found in random order so here we test for presence
        of various named atoms in the entire set of results...
        """
        self.intD = InteractionDetector("hsg1V.pdbqt", detect_pi=True)
        resStr = self.intD.processLigand("clean.pdbqt", outputfilename='filtered_clean.pdbqt')
        #self.assertEqual(resStr, 'lig_close_atoms:18\nlig_hb_atoms:5\npi_cation:1\n')
        resStr_list = resStr.split('\n')
        all_strs = ["USER  AD> lig_hb_atoms : 5", 'USER  AD> clean: : INI 20:O4,H3~hsg1V:B:ASP29:OD2', ]
        all_strs.append('USER  AD> clean: : INI 20:O2,H2~hsg1V:B:ASP25:OD1')
        all_strs.append('USER  AD> hsg1V:B:ARG8:NH2,HH22~clean: : INI 20:N5')
        all_strs.append('USER  AD> hsg1V:B:ASP29:N,HN~clean: : INI 20:O4')
        all_strs.append('USER  AD> clean: : INI 20:N4,H3~hsg1V:B:GLY27:O')
        all_strs.append('USER  AD> macro_close_ats: 25')
        all_strs.append('USER  AD> lig_close_ats: 18')
        all_strs.append('USER  AD> clean: : INI 20:') #H3,C1,O2,C2,C2,C8,H3,C9,C2,C1,C3,N5,C2,C2,C1,H2,C3,O4')
        #all_strs.append('USER clean: : INI 20:C1,O2,C2,C8,H3,C9,C2,C1,H3,C3,C2,O4,C2,C1,C2,H2,N5,C3')
        #all_strs.append('USER clean: : INI 20:O4,C1,O2,C2,C2,C8,H3,C9,C1,H3,C2,C3,N5,C2,C2,C1,H2,C3')
        #all_strs.append('USER clean: : INI 20:C1,O2,C2,C2,C8,H3,C9,C1,H3,C3,C2,C2,O4,C2,N5,C1,H2,C3')
        #all_strs.append('USER hsg1V:B:ILE84:CD1;hsg1V:B:PRO81:CB;hsg1V:A:ASP25:CG;hsg1V:B:VAL32:CG2;hsg1V:A:ARG8:NH2;hsg1V:B:ASP30:HN;hsg1V:B:ASP29:CG;hsg1V:A:ARG8:CZ;hsg1V:B:ARG8:HH22;hsg1V:A:ARG8:HH22;hsg1V:B:ASP30:CB;hsg1V:B:VAL82:CG2;hsg1V:A:ASP25:OD1;hsg1V:A:VAL82:CG1;hsg1V:B:ASP25:CG;hsg1V:B:ASP29:HN;hsg1V:B:ALA28:CB;hsg1V:B:ASP30:O;hsg1V:B:ASP30:OD2;hsg1V:B:ASP25:OD1;hsg1V:A:ARG8:HH21;hsg1V:B:ASP25:OD2;hsg1V:B:GLY27:O;hsg1V:B:PRO81:CG;hsg1V:B:ASP29:OD2')
        self.assertEqual(resStr_list[0], "USER  AD> lig_hb_atoms : 5")
        self.assertEqual(resStr_list[1] in all_strs, True)
        self.assertEqual(resStr_list[2] in all_strs, True)
        self.assertEqual(resStr_list[3] in all_strs, True)
        self.assertEqual(resStr_list[4] in all_strs, True)
        self.assertEqual(resStr_list[5] in all_strs, True)
        self.assertEqual(resStr_list[6] in all_strs, True)
        self.assertEqual(resStr_list[8] in all_strs, True)
        print resStr_list[9][:21] 
        #self.assertEqual(resStr_list[9][:21] in all_strs, True)
        #self.assertEqual(resStr_list[7] in all_strs, True)
        #\nhsg1V:B:ARG8:NH2,HH22~clean: : INI 20:N5\nclean: : INI 20:O4,H3~hsg1V:B:ASP29:OD2\nhsg1V:B:ASP29:N,HN~clean: : INI 20:O4\nclean: : INI 20:N4,H3~hsg1V:B:GLY27:O\nclean: : INI 20:O2,H2~hsg1V:B:ASP25:OD1\nmacro_close_ats: 25\nUSER hsg1V:B:GLY27:O;hsg1V:B:ASP25:CG;hsg1V:A:ARG8:HH21;hsg1V:B:ASP25:OD1;hsg1V:B:ASP29:CG;hsg1V:B:ASP29:HN;hsg1V:B:PRO81:CB;hsg1V:A:ARG8:HH22;hsg1V:B:ASP25:OD2;hsg1V:B:ASP30:CB;hsg1V:B:PRO81:CG;hsg1V:B:ALA28:CB;hsg1V:A:ASP25:CG;hsg1V:A:ARG8:NH2;hsg1V:A:ASP25:OD1;hsg1V:B:ASP30:OD2;hsg1V:B:VAL82:CG2;hsg1V:B:ASP30:O;hsg1V:B:ARG8:HH22;hsg1V:B:VAL32:CG2;hsg1V:B:ILE84:CD1;hsg1V:B:ASP30:HN;hsg1V:B:ASP29:OD2;hsg1V:A:VAL82:CG1;hsg1V:A:ARG8:CZ\nlig_close_ats: 18\nUSER clean: : INI 20:C3,H3,C1,N5,C2,C1,H3,H2,C8,C2,O4,C2,C2,C1,C2,C3,O2,C9\n")


    def test_no_interactions_InteractionDetector3(self):
        """
        
        """
        self.intD = InteractionDetector("1dwd_rec.pdbqt", detect_pi=True)
        resStr = self.intD.processLigand("clean.pdbqt", outputfilename='no_interactions_clean.pdbqt')
        self.assertEqual(resStr, 'USER  AD> lig_hb_atoms : 0\nUSER  AD> macro_close_ats: 0\nUSER  AD> lig_close_ats: 0\n')



if __name__ == '__main__':
    #unittest.main()
    test_cases = [
        'InteractionDetectorTests',
            ]
    unittest.main( argv=([__name__ ,] + test_cases) )


    
