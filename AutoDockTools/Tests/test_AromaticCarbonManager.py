#
#
#
#
# $Id: test_AromaticCarbonManager.py,v 1.2 2004/08/25 20:47:52 rhuey Exp $
#

import unittest
from AutoDockTools.atomTypeTools import AromaticCarbonManager


class BaseTests(unittest.TestCase):
    def setUp(self):
        from MolKit import Read
        self.mol = Read('ligandfiles/mtx.mol2')[0]
        self.mol.buildBondsByDistance()
        self.mol.allAtoms.autodock_element = self.mol.allAtoms.element
    

    def tearDown(self):
        """
        clean-up
        """
        del(self.mol)


    def test_constructor(self):
        """
        instantiate an AromaticCarbonManager
        """
        ACM = AromaticCarbonManager()
        self.assertEquals(ACM.__class__, AromaticCarbonManager)



    def test_setAromaticCarbons(self):
        """
         test that AromaticCarbonManager sets AromaticCarbons using cutoff
        """
        ACM = AromaticCarbonManager()
        aromCs_default = ACM.setAromaticCarbons(self.mol)
        self.assertEquals(len(aromCs_default), 0)
        aromCs_10 = ACM.setAromaticCarbons(self.mol, cutoff=10)
        self.assertEquals(len(aromCs_10), 6)
        aromCs_40 = ACM.setAromaticCarbons(self.mol, cutoff=40)
        self.assertEquals(len(aromCs_40), 12)


    def test_change_aromatic_criteria(self):
        """
         test change_aromatic_criteria
        """
        ACM = AromaticCarbonManager()
        aromCs_default = ACM.setAromaticCarbons(self.mol)
        self.assertEquals(len(aromCs_default), 0)
        ACM.change_aromatic_criteria(40)
        aromCs_40 = ACM.setAromaticCarbons(self.mol)
        self.assertEquals(len(aromCs_40), 12)


    def test_set_carbon_names(self):
        """
         test setting carbon names
        """
        ACM = AromaticCarbonManager()
        aromCs_40 = ACM.setAromaticCarbons(self.mol, cutoff=40)
        otherCs = self.mol.allAtoms.get(lambda x: x.autodock_element=='C')
        aliphaticCs = ACM.set_carbon_names(aromCs_40, 'C')
        self.assertEqual(len(aromCs_40), len(aliphaticCs))
        newCs = self.mol.allAtoms.get(lambda x: x.autodock_element=='C')
        self.assertEqual(len(newCs), len(otherCs + aliphaticCs))
        aromCs = ACM.set_carbon_names(aromCs_40, 'A')
        #test setting them back to Cs
        As = self.mol.allAtoms.get(lambda x: x.autodock_element=='A')
        self.assertEqual(len(aromCs_40), len(As))


if __name__ == '__main__':
    unittest.main()
