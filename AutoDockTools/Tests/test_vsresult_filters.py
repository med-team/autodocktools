#
#
#
#
# $Header: /opt/cvs/python/packages/share1.5/AutoDockTools/Tests/test_vsresult_filters.py,v 1.7 2010/07/08 16:14:10 rhuey Exp $
#
# $Id: test_vsresult_filters.py,v 1.7 2010/07/08 16:14:10 rhuey Exp $
#
#
#
# 
#

import unittest, os, sys
from AutoDockTools.VSResultFilters import Filter, EnergyFilter, ClusterSizeFilter, ClusterPercentageFilter,\
                                     EnergyClusterSizeFilter, BestEnergyInLargestCluster, \
                                     EnergyPlusBestEnergyInLargestCluster, LigandEfficiencyFilter, \
                                     CloseContactInteractionFilter, HydrogenBondInteractionFilter, \
                                     EnergyRangeFilter, EnergyInLargestCluster


class FilterBaseTests(unittest.TestCase):
    def setUp(self):
        """
        instantiate an EnergyFilter
        """
        filter = Filter()
        self.assertEquals(filter.__class__, Filter)

        
class EnergyFilterTests(FilterBaseTests):

    def test_PassingEnergyFilter(self):
        """
        lowest energy in VSResult -7.47 passes compared to -7
        """
        self.ef = EnergyFilter(-7)
        self.assertEqual(self.ef.filter("ZINC02026663_vs.pdbqt"), True)

    def test_PassingEnergyFilter2(self):
        """
        lowest energy in VSResult -7.47 passes compared to -7.40
        """
        self.ef = EnergyFilter(-7.40)
        self.assertEqual(self.ef.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingEnergyFilter(self):
        """
        lowest energy in VSResult -7.47 fails compared to -12
        """
        self.ef = EnergyFilter(-12)
        self.assertEqual(self.ef.filter("ZINC02026663_vs.pdbqt"), False)


class EnergyRangeFilterTests(FilterBaseTests):

    def test_PassingEnergyRangeFilter(self):
        """
        lowest energy in VSResult -7.47 passes compared to range -8 to -7
        """
        self.erf = EnergyRangeFilter(-8, -7)
        self.assertEqual(self.erf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_PassingEnergyRangeFilter2(self):
        """
        lowest energy in VSResult -7.47 passes compared to -7.50 to -7.4
        """
        self.erf = EnergyRangeFilter(-7.5, -7.40)
        self.assertEqual(self.erf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingEnergyRangeFilter(self):
        """
        lowest energy in VSResult -7.47 fails compared to -12 to -8
        """
        self.erf = EnergyRangeFilter(-12, -8)
        self.assertEqual(self.erf.filter("ZINC02026663_vs.pdbqt"), False)


    #def test_InvalidEnergyRangeFilter(self):
    #    """
    #    invalid min_energy and max_energy values raises an error...
    #    """
    #    try:
    #        self.assertRaises(RuntimeError,  EnergyRangeFilter(-8, -12))
    #        self.assertEqual(1, 1)
    #    except:
    #        print "in except"



        
class ClusterSizeFilterTests(FilterBaseTests):

    def test_PassingClusterSizeFilter(self):
        """
        largest cluster size in VSResult 39 passes compared to 20
        """
        self.csf = ClusterSizeFilter(cluster_size=20)
        self.assertEqual(self.csf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_PassingClusterSizeFilterInclusiveDefault(self):
        """
        largest cluster size in VSResult 39 passes compared to 39 default inclusive True
        """
        self.csf = ClusterSizeFilter(cluster_size=39)
        self.assertEqual(self.csf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingClusterSizeFilterNonInclusive(self):
        """
        largest cluster size in VSResult 39 fails compared to 39 in not inclusive
        """
        self.csf = ClusterSizeFilter(cluster_size=39, inclusive=False)
        self.assertEqual(self.csf.filter("ZINC02026663_vs.pdbqt"), False)


    def test_FailingClusterSizeFilter(self):
        """
        largest cluster size in VSResult 39 fails compared to 40
        """
        self.csf = ClusterSizeFilter(cluster_size=40)
        self.assertEqual(self.csf.filter("ZINC02026663_vs.pdbqt"), False)


        
class ClusterPercentageFilterTests(FilterBaseTests):

    def test_PassingClusterPercentageFilterTests(self):
        """
        percentage in cluster in VSResult passes compared to 16
        """
        self.csf = ClusterPercentageFilter(percentage=16)
        self.assertEqual(self.csf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_PassingClusterPercentageFilterTests2(self):
        """
        percentage in cluster in VSResult passes compared to 30
        """
        self.csf = ClusterPercentageFilter(percentage=30)
        self.assertEqual(self.csf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingClusterPercentageFilterTests(self):
        """
        percentage in cluster in VSResult passes compared to 33
        """
        self.csf = ClusterPercentageFilter(percentage=33)
        self.assertEqual(self.csf.filter("ZINC02026663_vs.pdbqt"), False)

    def test_ZINC02027973_vs_PassingClusterPercentageFilterTests(self):
        """
        percentage in cluster in VSResult passes compared to 16
        """
        self.csf = ClusterPercentageFilter(percentage=16)
        self.assertEqual(self.csf.filter("ZINC02027973_vs.pdbqt"), True)


    def test_ZINC02027973_vs_PassingClusterPercentageFilterTests2(self):
        """
        percentage in cluster in VSResult passes compared to 30
        """
        self.csf = ClusterPercentageFilter(percentage=34)
        self.assertEqual(self.csf.filter("ZINC02027973_vs.pdbqt"), True)



    def test_ZINC02027973_vs_FailingClusterPercentageFilterTests(self):
        """
        percentage in cluster in VSResult fails compared to 33
        """
        self.csf = ClusterPercentageFilter(percentage=36)
        self.assertEqual(self.csf.filter("ZINC02027973_vs.pdbqt"), False)


    def test_ZINC02027973_vs_PassingClusterPercentage0FilterTests(self):
        """
        percentage in cluster in VSResult passes if compared to 0
        """
        self.csf = ClusterPercentageFilter(percentage=0)
        self.assertEqual(self.csf.filter("ZINC02027973_vs.pdbqt"), True)

class EnergyClusterSizeFilterTests(FilterBaseTests):

    def test_PassingEnergyClusterSizeFilter(self):
        """
        energy -7 and cluster_size 30 pass compared to -7.47 and 39 in dlg 
        """
        self.ecsf = EnergyClusterSizeFilter(energy=-7, cluster_size=30)
        self.assertEqual(self.ecsf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingEnergyClusterSizeFilter(self):
        """
        energy -8 and cluster_size 9 fail compared to -7.47 and 39 in dlg 
        """
        self.ecsf = EnergyClusterSizeFilter(energy=-8, cluster_size=9)
        self.assertEqual(self.ecsf.filter("ZINC02026663_vs.pdbqt"), False)


    def test_FailingEnergyClusterSizeFilter2(self):
        """
        energy -7 and cluster_size 60 fail compared to -7.47 and 39 in dlg 
        """
        self.ecsf = EnergyClusterSizeFilter(energy=-7, cluster_size=60)
        self.assertEqual(self.ecsf.filter("ZINC02026663_vs.pdbqt"), False)



class BestEnergyInLargestClusterTests(FilterBaseTests):

    def test_PassingBestEnergyInLargestCluster(self):
        """
        cluster containing best energy conformation is also the largest cluster 
        """
        self.ecsf = BestEnergyInLargestCluster()
        self.assertEqual(self.ecsf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingBestEnergyInLargestCluster(self):
        """
        cluster containing best energy conformation is NOT also the largest cluster 
        """
        self.ecsf = BestEnergyInLargestCluster()
        self.assertEqual(self.ecsf.filter("ZINC02025973_vs_le.pdbqt"), False)



class LigandEfficiencyFilterTests(FilterBaseTests):

    def test_PassingLigandEfficiencyFilter(self):
        """
        LigandEfficiency is the best overall energy/number of atoms in the ligand 
        """
        self.ecsf = LigandEfficiencyFilter(-0.25) 
        self.assertEqual(self.ecsf.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingLigandEfficiencyFilter(self):
        """
        cluster containing best energy conformation is NOT also the largest cluster 
        """
        self.ecsf2 = LigandEfficiencyFilter(-0.5)
        self.assertEqual(self.ecsf2.filter("ZINC02026663_vs.pdbqt"), False)


class CloseContactInteractionFilterTests(FilterBaseTests):

    def test_PassingCloseContactInteractionFilter(self):
        """
        two atoms interact if the distance between them is less than the sum
        of their vdw radii
        """
        self.intF = CloseContactInteractionFilter("xJ1_xtal:B:LYS55")
        self.assertEqual(self.intF.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingCloseContactInteractionFilter(self):
        """
        no close contact interactions between docked ligand and xJ1_xtal:A:LYS55
        """
        self.intF = CloseContactInteractionFilter("xJ1_xtal:A:LYS55")
        self.assertEqual(self.intF.filter("ZINC02026663_vs.pdbqt"), False)
        

    def test_PassingCloseContactInteractionFilter2(self):
        """
        two atoms interact if the distance between them is less than the sum
        of their vdw radii
        """
        self.intF2 = CloseContactInteractionFilter("xJ1_xtal:B:GLU35:OE2")
        self.assertEqual(self.intF2.filter("ZINC02026663_vs.pdbqt"), True)



class HydrogenBondInteractionFilterTests(FilterBaseTests):

    def test_PassingHydrogenBondInteractionFilter(self):
        """
        two atoms interact if the distance between them is less than the sum
        of their vdw radii
        """
        self.intF = HydrogenBondInteractionFilter("xJ1_xtal:B:LYS55")
        self.assertEqual(self.intF.filter("ZINC02026663_vs.pdbqt"), True)


    def test_FailingHydrogenBondInteractionFilter(self):
        """
        no hydrogenbond interactions between docked ligand and xJ1_xtal:A:LYS55
        """
        self.intF = HydrogenBondInteractionFilter("xJ1_xtal:A:LYS5")
        self.assertEqual(self.intF.filter("ZINC02026663_vs.pdbqt"), False)
        

class EnergyInLargestClusterTests(FilterBaseTests):

    def test_PassingEnergyInLargestCluster(self):
        """
        largest cluster best energy meets an energy criterion..
        """
        self.eilc = EnergyInLargestCluster(-5.3)
        self.assertEqual(self.eilc.filter("ZINC02025973_vs_lc.pdbqt"), True)


    def test_FailingBestEnergyInLargestCluster(self):
        """
        cluster containing best energy conformation is NOT also the largest cluster 
        """
        self.eilc = EnergyInLargestCluster(-5.4)
        self.assertEqual(self.eilc.filter("ZINC02025973_vs_lc.pdbqt"), False)



if __name__ == '__main__':
    #unittest.main()
    test_cases = [
        'EnergyFilterTests',
        'ClusterSizeFilterTests',
        'ClusterPercentageFilterTests',
        'EnergyClusterSizeFilterTests',
        'BestEnergyInLargestClusterTests',
        'LigandEfficiencyFilterTests',
        'CloseContactInteractionFilterTests',
        'HydrogenBondInteractionFilterTests',
        'EnergyInLargestClusterTests',
            ]
    unittest.main( argv=([__name__ ,] + test_cases) )


    
