#
#
#  $Id: test_LibraryPfWriters.py,v 1.1 2004/06/17 21:40:07 rhuey Exp $
#
#

import unittest
import glob, os, string
from AutoDockTools.lib_parameter_file_writer import LibraryGpfWriter, LibraryDpfWriter 



class LibraryGridParamFileWriter(unittest.TestCase):
    """
    class to test writing Autogrid Parameter files for all ligand types in a
    library
    """

    def setUp(self):
        """
        initialize
        """
        self.gpffile = 'hsg1.gpf'
        self.writer = LibraryGpfWriter(self.gpffile, extension = "out.pdbq", 
                                        prefix="test_lgp_")
        self.atomtypes = ['A','H','C','O','N']


    def tearDown(self):
        """
        clean-up
        """
        cmd = "rm -f test_lgp_*.gpf"
        os.system(cmd)
        delattr(self, 'writer')
        

    def test_write_file0(self):
        """
        check code doesnot crash
        """
        self.writer.write(self.writer.gpo)
        self.assertEqual(1, 1)


    def test_write_file(self):
        """
        check gpf file is created
        """
        self.writer.write(self.writer.gpo)
        outputfilename = "test_lgp_hsg1_0_library.gpf"
        self.assertEqual(os.path.exists(outputfilename), True)



class LibraryDockingParamFileWriter(unittest.TestCase):
    """
    class to test writing docking parameter files for all ligand files in a
    library
    """

    def setUp(self):
        """
        initialize
        """
        self.dpffile = 'hsg1_ind.dpf'
        self.writer = LibraryDpfWriter(self.dpffile, extension = "out.pdbq",
                                               prefix = "test_ldp_")
        self.files_to_remove = []
        for k in ['C','A','N','O','H','S']:
            mapname = 'hsg1.'+ k +'.map'
            if not os.path.exists(mapname):
                cmd = 'touch ' + mapname
                #print "touched ", mapname
                os.system(cmd)
                self.files_to_remove.append(mapname)
        


    def tearDown(self):
        """
        clean-up
        """
        cmd = "rm -f test_ldp_*"
        os.system(cmd)
        for f in self.files_to_remove:
            cmd = "rm -f " + f
            os.system(cmd)
            #print "removed ", f
        delattr(self, 'writer')
        

    def test_write_file0(self):
        """
        check code does not crash
        """
        self.writer.write(self.writer.dpo)
        self.assertEqual(1, 1)


    def test_write_file(self):
        """
        check dpf files are created
        """
        self.writer.write(self.writer.dpo, 'GALS')
        GALSfiles = glob.glob("*GALS.dpf")
        self.assertEqual(len(GALSfiles)>0, True)


    def test_no_errors_written(self):
        """
        check write_lib_dpf_errors is empty
        """
        self.writer.write(self.writer.dpo, 'GALS')
        errptr= open("write_lib_dpf_errors")
        self.assertEqual(len(errptr.readlines()), 0)


    def test_correct_number_of_files_written(self):
        """
        check correct number of dpf files are written
        """
        #get old number of dpf files
        old_number_dpffiles = len(glob.glob("*GALS.dpf"))
        #get number of ligand files
        key = "*" + self.writer.extension
        number_ligand_files = len(glob.glob(key))
        #write dpffiles
        self.writer.write(self.writer.dpo, 'GALS')
        #get new number of dpf files
        new_number_dpffiles = len(glob.glob("*GALS.dpf"))
        self.assertEqual(new_number_dpffiles, number_ligand_files + old_number_dpffiles)


if __name__ == '__main__':
    unittest.main()




