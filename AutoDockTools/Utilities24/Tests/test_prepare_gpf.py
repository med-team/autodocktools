#
#
#
#
# $Id: test_prepare_gpf.py,v 1.1 2005/07/21 22:47:02 sowjanya Exp $
#

import unittest, os, sys
from string import split, find
from MolKit import Read
from AutoDockTools.GridParameters import GridParameters

class BaseTests(unittest.TestCase):

    def test_default(self):
        """
    prepare_gpf.py -l pdbq_file -r pdbqs_file -"
       [-l ligand_filename]"
       [-r receptor_filename]"
   Optional parameters:"
       [-i reference_gpf_filename]"
       [-o output_gpf_filename]"
       [-p parameter=newvalue]"
       [-v]"

        """
        filename = "test_default.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf.py -l ind.pdbq -r hsg1.pdbqs -o " + filename 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), 1)


    def test_use_reference_gpf(self):
        """
        test setting parameters using reference gpf
        options
           -i gpffilename
        """
        filename = "test_ref.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd =  cmd + "; ../prepare_gpf.py -l ind.pdbq -r hsg1.pdbqs -i hsg1.gpf -o " + filename
        os.system(cmd)
        ref_gpo = GridParameters()
        ref_gpo.read('hsg1.gpf')
        gpo = GridParameters()
        gpo.read(filename)
        for key,refval in ref_gpo.items():
            if gpo[key]['value']!=refval['value']:
                print "failing.. ", key
            self.assertEqual(gpo[key]['value'], refval['value'])


    def test_setting_parameter(self):
        """
        test setting parameter directly
        options
           -p parameter=newvalue
        """
        filename = "test_param.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf.py -l ind.pdbq -r hsg1.pdbqs -p spacing=1.0  -o " + filename
        os.system(cmd)
        gpo = GridParameters()
        gpo.read(filename)
        self.assertEqual(gpo['spacing']['value'], 1.0) 


    def test_setting_2_parameters(self):
        """
        test setting 2 parameters directly
        options
           -p parameter=newvalue
           -p parameter2=newvalue2
        """
        filename = "test_2params.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf.py -l ind.pdbq -r hsg1.pdbqs -p spacing=1.0 -p gridcenter='1.0 2.0 3.0' -o " + filename
        os.system(cmd)
        gpo = GridParameters()
        gpo.read(filename)
        self.assertEqual(gpo['spacing']['value'], 1.0) 
        self.assertEqual(int(gpo['gridcenter']['value'][0]), 1.0) 


if __name__ == '__main__':
    unittest.main()
