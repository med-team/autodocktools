#
#
#
#
# $Id: test_prepare_gpf4.py,v 1.4.2.1 2015/08/26 22:45:31 sanner Exp $
#

import unittest, os, sys
from MolKit import Read
from AutoDockTools.GridParameters import GridParameters

class BaseTests(unittest.TestCase):

    def test_default(self):
        """
    prepare_gpf4.py -l pdbqt_file -r pdbqt_file -"
       [-l ligand_filename]"
       [-r receptor_filename]"
   Optional parameters:"
       [-i reference_gpf_filename]"
       [-o output_gpf_filename]"
       [-p parameter=newvalue]"
       [-v]"

        """
        filename = "test_default4.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf4.py -l 1ebg_lig.pdbqt -r 1ebg_rec.pdbqt -o " + filename 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), 1)

    def test_set_ligand_types_directly_gpf(self):
        """
        test setting ligand_types parameters using list instead of ligand
        options
           -p ligand_types=
        """
        filename = "test_set_ligand_types_directly.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf4.py -p ligand_types=P,C,OA,HD,N  -r 1ebg_rec.pdbqt -i ref_1ebg.gpf -o " + filename
        os.system(cmd)
        ref_gpo = GridParameters()
        ref_gpo.read4('ref_1ebg.gpf')
        gpo = GridParameters()
        gpo.read4(filename)
        for key,refval in ref_gpo.items():
            if gpo[key]['value']!=refval['value']:
                print "failing.. ", key, '  newvalue=', gpo[key]['value']
                print '  refvalue=', ref_gpo[key]['value']
            self.assertEqual(gpo[key]['value'], refval['value'])

    def test_use_reference_gpf(self):
        """
        test setting parameters using reference gpf
        options
           -i gpffilename
        """
        filename = "test_ref4.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf4.py -l 1ebg_lig.pdbqt -r 1ebg_rec.pdbqt -i ref_1ebg.gpf -o " + filename
        os.system(cmd)
        ref_gpo = GridParameters()
        ref_gpo.read4('ref_1ebg.gpf')
        gpo = GridParameters()
        gpo.read4(filename)
        for key,refval in ref_gpo.items():
            if gpo[key]['value']!=refval['value']:
                print "failing.. ", key, '  newvalue=', gpo[key]['value']
                print '  refvalue=', ref_gpo[key]['value']
            self.assertEqual(gpo[key]['value'], refval['value'])


    def test_setting_parameter(self):
        """
        test setting parameter directly
        options
           -p parameter=newvalue
        """
        filename = "test_param4.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf4.py -l 1ebg_lig.pdbqt -r 1ebg_rec.pdbqt -p spacing=1.0  -o " + filename
        os.system(cmd)
        gpo = GridParameters()
        gpo.read4(filename)
        self.assertEqual(gpo['spacing']['value'], 1.0) 


    def test_setting_2_parameters(self):
        """
        test setting 2 parameters directly
        options
           -p parameter=newvalue
           -p parameter2=newvalue2
        """
        filename = "test_2params4.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf4.py -l 1ebg_lig.pdbqt -r 1ebg_rec.pdbqt -p spacing=1.0 -p gridcenter='1.0 2.0 3.0' -o " + filename
        os.system(cmd)
        gpo = GridParameters()
        gpo.read4(filename)
        self.assertEqual(gpo['spacing']['value'], 1.0) 
        self.assertEqual(int(gpo['gridcenter']['value'][0]), 1.0) 


    def test_setting_parameter_file_parameter(self):
        """
        test setting parameter_file directly
        options
           -p parameter_file='ad4_local.dat'
        """
        filename = "test_setting_parameter_file.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf4.py -l 1ebg_lig.pdbqt -r 1ebg_rec.pdbqt -p 'parameter_file=ad4_local.dat' -o " + filename
        os.system(cmd)
        gpo = GridParameters()
        gpo.read4(filename)
        self.assertEqual(gpo['custom_parameter_file']['value'], 1.0) 
        self.assertEqual(gpo['parameter_file']['value'], 'ad4_local.dat') 

    def test_center_on_ligand(self):
        """
        test center on ligand
        options
           -y center_on_ligand=True
        """
        filename = "test_center_on_ligand.gpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../prepare_gpf4.py -l 1ebg_lig.pdbqt -r 1ebg_rec.pdbqt -y -o " + filename
        os.system(cmd)
        gpo = GridParameters()
        gpo.read4(filename)
        self.assertEqual(gpo['gridcenter']['value'],  [17.646, 5.470, 20.124])
        fptr = open("test_center_on_ligand.gpf")
        lines = fptr.readlines()
        fptr.close()
        gridcenter_line = lines[6].strip()
        self.assertEqual(gridcenter_line, "gridcenter 17.646 5.470 20.124       # xyz-coordinates or auto")



if __name__ == '__main__':
    unittest.main()
