#
#
#
#
# $Id: test_prepare_dpf.py,v 1.6 2012/05/21 18:44:35 rhuey Exp $
#

import unittest, os, sys
from time import sleep
from string import split, find
from MolKit import Read
from AutoDockTools.DockingParameters import DockingParameters

class BaseTests(unittest.TestCase):

    def test_default(self):
        """
    prepare_dpf.py -l pdbq_file -r pdbqs_file -"
       [-l ligand_filename]"
       [-r receptor_filename]"
   Optional parameters:"
       [-i reference_dpf_filename]"
       [-o output_dpf_filename]"
       [-p parameter=newvalue]"
       [-v verbose output]"

        """
        filename = "test_default.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o " + filename 
        #cmd = "../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o " + filename 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_use_reference_dpf(self):
        """
        test setting parameters using reference dpf
        options
           -i dpffilename
        """
        filename = "test_ref.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -i hsg1_ind.dpf -o " + filename
        #cmd = "../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -i hsg1_ind.dpf -o " + filename
        os.system(cmd)
        #sleep(5)
        ref_dpo = DockingParameters()
        ref_dpo.read('ref_GALS.dpf')
        print "ref_dpo['ga_num_evals']['value'] =", ref_dpo['ga_num_evals']['value']
        dpo = DockingParameters()
        dpo.read(filename)
        for key, refval in ref_dpo.items():
            #skip keys specific for each ligand
            if key not in ['move', 'ndihe']:
                if dpo[key]['value']!=refval['value']:
                    print "failing.. ", key
                self.assertEqual(dpo[key]['value'], refval['value'])
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_setting_parameter(self):
        """
        test setting parameter directly
        options
           -p parameter=newvalue
        """
        filename = "test_param.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -p ga_num_evals=100000000  -o " + filename
        #cmd = "../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -p ga_num_evals=100000000  -o " + filename
        os.system(cmd)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo['ga_num_evals']['value'], 100000000) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_setting_2_parameters(self):
        """
        test setting 2 parameters directly
        options
           -p parameter=newvalue
           -p parameter2=newvalue2
        """
        filename = "test_2params.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -p ga_pop_size=200 -p ga_num_evals=1234567 -o " + filename
        #cmd = "../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -p ga_pop_size=200 -p ga_num_evals=1234567 + filename
        os.system(cmd)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo['ga_pop_size']['value'], 200) 
        self.assertEqual(int(dpo['ga_num_evals']['value']), 1234567) 
        #if os.path.exists(filename):
        #    cmd = "rm  " + filename
        #    os.system(cmd)


    def test_setting_3_parameters(self):
        """
        test setting 3 parameters directly
        options
           -p parameter=newvalue
           -p parameter2=newvalue2
           -p parameter3=newvalue3
        """
        filename = "test_3params.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -p ga_pop_size=200 -p set_sw1=0 -p set_psw1=1  -o " + filename
        #cmd = "../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -p ga_pop_size=200 -p about='1.0 2.0 3.0' -o " + filename
        os.system(cmd)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo['ga_pop_size']['value'], 200) 
        self.assertEqual(int(dpo['set_psw1']['value']), 1) 
        self.assertEqual(dpo['set_sw1']['value'],0)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)

# test each parameter separately
    def test_outlev(self):
        """
        test setting outlev
        """
        filename = "test_outlev.dpf"
        param = 'outlev'
        val = 6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_seed(self):
        """
        test setting seed
        """
        filename = "test_seed.dpf"
        param = 'seed'
        val = [1234,56789]
        testval = "[1234,56789]"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_types(self):
        """
        test setting types
        """
        filename = "test_types.dpf"
        param = 'types'
        val = testval = "ACHN"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_fld(self):
        """
        test setting fld
        NB: This is normally set by specifying the receptor filename 
        """
        filename = "test_fld.dpf"
        param = 'fld'
        val = testval = "hsg1.maps.fld"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_map(self):
        """
        test setting map
        NB: This is normally set by specifying the receptor filename 
        """
        filename = "test_map.dpf"
        param = 'map'
        val = testval = "hsg1.e.map"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_move(self):
        """
        test setting move
        NB: This is normally set by specifying the ligand filename 
        """
        filename = "test_move.dpf"
        param = 'move'
        val = testval = "ind.pdbq"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_about(self):
        """
        test setting about
        """
        filename = "test_about.dpf"
        param = 'about'
        val = [1.0,1.0,1.0]
        #testval = str(val)
        testval = "[1.0,1.0,1.0]"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_tran0(self):
        """
        test setting tran0
        """
        filename = "test_about.dpf"
        param = 'tran0'
        val = [1.0,2.0,3.0]
        #testval = str(val)
        testval = "[1.0,2.0,3.0]"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_quat0(self):
        """
        test setting quat0
        """
        filename = "test_about.dpf"
        param = 'quat0'
        val = [1.0,2.0,3.0,4.0]
        #testval = str(val)
        testval = "[1.0,2.0,3.0,4.0]"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ndihe(self):
        """
        test setting ndihe
        NB: This is normally set by specifying the ligand filename 
        """
        filename = "test_ndihe.dpf"
        param = 'ndihe'
        val = 12
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_dihe0(self):
        """
        test setting dihe0
        """
        filename = "test_about.dpf"
        param = 'dihe0'
        val = [1.0,2.0,3.0,4.0]
        #testval = str(val)
        testval = "[1.0,2.0,3.0,4.0]"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_sw_max_its(self):
        """
        test setting sw_max_its
        """
        filename = "test_sw_max_its.dpf"
        param = 'sw_max_its'
        val = 123
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_sw_max_succ(self):
        """
        test setting sw_max_succ
        """
        filename = "test_sw_max_succ.dpf"
        param = 'sw_max_succ'
        val = 321
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_sw_max_fail(self):
        """
        test setting sw_max_fail
        NB: This is normally set by specifying the ligand filename 
        """
        filename = "test_sw_max_fail.dpf"
        param = 'sw_max_fail'
        val = 222 
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_elecmap(self):
        """
        test not being able to set elecmap
        NB: This is ALWAYS set by specifying the receptor filename 
        and value remains ''
        """
        filename = "test_elecmap.dpf"
        param = 'elecmap'
        val = testval = "hsg1.e.map"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        #THIS IS AN EXCEPTION
        self.assertEqual(dpo[param]['value'], '') 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_torsdof(self):
        """
        test setting torsdof
        NB: This is normally set by specifying the ligand filename 
        """
        filename = "test_torsdof.dpf"
        param = 'torsdof'
        val = [12,0.3113]
        #testval = str(val)
        testval = "[12,0.3113]"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_intelec(self):
        """
        test setting intelec
        """
        filename = "test_intelec.dpf"
        param = 'intelec'
        val = 1
        #testval = str(val)
        testval = '1'
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_rmstol(self):
        """
        test setting rmstol
        """
        filename = "test_rmstol.dpf"
        param = 'rmstol'
        val = 0.10
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_extnrg(self):
        """
        test setting extnrg
        """
        filename = "test_extnrg.dpf"
        param = 'extnrg'
        val = 1000000
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_e0max(self):
        """
        test setting e0max
        NB: This is normally set by specifying the ligand filename 
        """
        filename = "test_e0max.dpf"
        param = 'e0max'
        val = [1.,31133]
        #testval = str(val)
        testval = "[1.,31133]"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_pop_size(self):
        """
        test setting ga_pop_size
        """
        filename = "test_ga_pop_size.dpf"
        param = 'ga_pop_size'
        val = 10
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_num_evals(self):
        """
        test setting ga_num_evals
        """
        filename = "test_ga_num_evals.dpf"
        param = 'ga_num_evals'
        val = 1000
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_num_generations(self):
        """
        test setting ga_num_generations
        """
        filename = "test_ga_num_generations.dpf"
        param = 'ga_num_generations'
        val = 10
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_elitism(self):
        """
        test setting ga_elitism
        """
        filename = "test_ga_elitism.dpf"
        param = 'ga_elitism'
        val = 0.0006
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_mutation_rate(self):
        """
        test setting ga_mutation_rate
        """
        filename = "test_ga_mutation_rate.dpf"
        param = 'ga_mutation_rate'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_crossover_rate(self):
        """
        test setting ga_crossover_rate
        """
        filename = "test_ga_crossover_rate.dpf"
        param = 'ga_crossover_rate'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_window_size(self):
        """
        test setting ga_window_size
        """
        filename = "test_ga_window_size.dpf"
        param = 'ga_window_size'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_cauchy_alpha(self):
        """
        test setting ga_cauchy_alpha
        """
        filename = "test_ga_cauchy_alpha.dpf"
        param = 'ga_cauchy_alpha'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ga_cauchy_beta(self):
        """
        test setting ga_cauchy_beta
        """
        filename = "test_ga_cauchy_beta.dpf"
        param = 'ga_cauchy_beta'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_set_ga(self):
        """
        test setting set_ga
        """
        filename = "test_set_ga.dpf"
        param = 'set_ga'
        val = 1
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_sw_max_its(self):
        """
        test setting sw_max_its
        """
        filename = "test_sw_max_its.dpf"
        param = 'sw_max_its'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_sw_max_succ(self):
        """
        test setting sw_max_succ
        """
        filename = "test_sw_max_succ.dpf"
        param = 'sw_max_succ'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_sw_rho(self):
        """
        test setting sw_rho
        """
        filename = "test_sw_rho.dpf"
        param = 'sw_rho'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_sw_lb_rho(self):
        """
        test setting sw_lb_rho
        """
        filename = "test_sw_lb_rho.dpf"
        param = 'sw_lb_rho'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_ls_search_freq(self):
        """
        test setting ls_search_freq
        """
        filename = "test_ls_search_freq.dpf"
        param = 'ls_search_freq'
        val = 0.6
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_set_psw1(self):
        """
        test setting set_psw1
        """
        filename = "test_set_psw1.dpf"
        param = 'set_psw1'
        val = 1
        testval = str(val)
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p %s=%s" %(filename, param, testval) 
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        dpo = DockingParameters()
        dpo.read(filename)
        self.assertEqual(dpo[param]['value'], val) 
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)


    def test_LS_dpf_compare_lines(self):
        """
        test setting 'L' flag to use local search only parameters
        """
        filename = "test_LS.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -L -o %s" %filename
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        testptr = open(filename)
        testlines = testptr.readlines()
        refptr = open("ref_LS.dpf")
        reflines = refptr.readlines()
        refptr.close()
        for refline,testline in zip(reflines,testlines):
            self.assertEqual(refline,testline)
            


    def test_SA_dpf_compare_lines(self):
        """
        test setting 'S' flag to use simulated annealing parameters
        """
        filename = "test_SA.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = "set PYTHONPATH=%s"%sys.path[0]
        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -S -o %s" %filename
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        testptr = open(filename)
        testlines = testptr.readlines()
        refptr = open("ref_SA.dpf")
        reflines = refptr.readlines()
        refptr.close()
        for refline,testline in zip(reflines,testlines):
            self.assertEqual(refline,testline)
            





##    THIS ONE DOES NOT WORK!!!
##    def test_do_global_only(self):
##        """
##        test setting do_global_only
##        """
##        filename = "test_do_global_only.dpf"
##        param = 'do_global_only'
##        val = 12
##        testval = str(val)
##        if os.path.exists(filename):
##            cmd = "rm  " + filename
##            os.system(cmd)
##        cmd = "set PYTHONPATH=%s"%sys.path[0]
##        cmd = cmd + ";../prepare_dpf.py -l ref_ind.out.pdbq -r hsg1.pdbqs -o %s -p ga_run=%s -p %s=%s" %(filename, testval, param, testval) 
##        os.system(cmd)
##        self.assertEqual(os.path.exists(filename), True)
##        dpo = DockingParameters()
##        dpo.read(filename)
##        self.assertEqual(dpo['ga_run']['value'], val) 
##        #self.assertEqual(dpo[param]['value'], val) 
##        self.assertEqual(dpo['ga_run']['value'], val) 
##        if os.path.exists(filename):
##            cmd = "rm  " + filename
##            os.system(cmd)



if __name__ == '__main__':
    unittest.main()
