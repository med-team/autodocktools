#
#
#
#
# $Id: test_pdbq_to_pdbqt.py,v 1.1 2005/07/21 22:47:02 sowjanya Exp $
#

import unittest, os, sys
from string import split, find
from MolKit import Read

class BaseTests(unittest.TestCase):

    def test_default_formatted_pdbq(self):
        """
    pdbq_to_pdbqt.py -s pdbq_stem -o outputfilename
       -s ligand_stem
       optional:
       [-o outputfilename]

        """
        filename = "test_formatted.pdbqt"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + '; ' + "../pdbq_to_pdbqt.py -s 1ebg_lig -o "+ filename
        #cmd = "../pdbq_to_pdbqt.py -s 1ebg_lig -o "+ filename
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        fptr = open('ref_formatted.pdbqt')
        reflines = fptr.readlines()
        fptr.close()
        fptr = open(filename)
        testlines = fptr.readlines()
        fptr = open(filename)
        for refline, testline in zip(reflines, testlines):
            if find(refline, 'REMARK')<0:
                self.assertEqual(refline, testline)


    def test_default_plain_pdbq(self):
        """
    pdbq_to_pdbqt.py -s pdbq_stem -o outputfilename
       -s ligand_stem
        """
        filename = "test_plain.pdbqt"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + '; ' + "../pdbq_to_pdbqt.py -s 1ebg -o "+ filename
        #cmd = "../pdbq_to_pdbqt.py -s 1ebg  -o " + filename
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        fptr = open('ref_plain.pdbqt')
        reflines = fptr.readlines()
        fptr.close()
        fptr = open(filename)
        testlines = fptr.readlines()
        fptr = open(filename)
        for refline, testline in zip(reflines, testlines):
            self.assertEqual(refline, testline)



if __name__ == '__main__':
    unittest.main()
