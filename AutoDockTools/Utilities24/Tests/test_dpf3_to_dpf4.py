#
#
#
#
# $Id: test_dpf3_to_dpf4.py,v 1.2 2009/03/04 16:53:59 rhuey Exp $
#

import unittest, os, sys
from string import split, find, strip
from MolKit import Read

class BaseTests(unittest.TestCase):

    def test_default_formatted_pdbq(self):
        """
    dpf3_to_dpf4.py -s dpf3_stem -r receptor_filename -l ligand_filename
       optional:
       [-o outputfilename]

        """
        filename = "test_new_dpf4.dpf"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + '; ' + "../dpf3_to_dpf4.py -s ref_ad3_dpf -r 1ebg_rec.pdbqt -l 1ebg_lig.pdbqt -o "+ filename
        #cmd = "../dpf3_to_dpf4.py -s ad3_1ebg -r 1ebg_rec.pdbqt -l 1ebg_lig.pdbqt -o "+ filename
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        fptr = open('ref_dpf4.dpf')
        reflines = fptr.readlines()
        fptr.close()
        fptr = open(filename)
        testlines = fptr.readlines()
        fptr = open(filename)
        for refline, testline in zip(reflines, testlines):
            self.assertEqual(strip(refline), strip(testline))



if __name__ == '__main__':
    unittest.main()
