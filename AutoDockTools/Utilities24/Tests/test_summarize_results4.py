#
#
#
#
# $Id: test_summarize_results4.py,v 1.2 2010/01/18 20:08:48 rhuey Exp $
#

import unittest, os, sys

class BaseTests(unittest.TestCase):

    def test_incomplete_dlg(self):
        """
    summarize_results4.py -d ."
    AutoDockTools/Tests directory contains failed docking 'partial.dlg'
        """
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + "; ../summarize_results4.py -d . " 
        result = os.system(cmd)
        print "result =", result
        self.assertEqual(result, 256)

if __name__ == '__main__':
    unittest.main()
