#
#
#
#
# $Id: test_pdbqs_to_pdbqt.py,v 1.3 2011/09/07 23:00:37 annao Exp $
#

import unittest, os, sys
from string import split, find
from MolKit import Read

class BaseTests(unittest.TestCase):

    def test_default_formatted_pdbq(self):
        """
    pdbqs_to_pdbqt.py -s pdbqs_stem -o outputfilename
       -s ligand_stem
       optional:
       [-o outputfilename]

        """
        filename = "test_hsg1.pdbqt"
        if os.path.exists(filename):
            cmd = "rm  " + filename
            os.system(cmd)
        cmd = 'set PYTHONPATH=%s'%sys.path[0]
        cmd = cmd + '; ' + "../pdbqs_to_pdbqt.py -s hsg1 -o "+ filename
        #cmd = "../pdbqs_to_pdbqt.py -s 1ebg_lig -o "+ filename
        os.system(cmd)
        self.assertEqual(os.path.exists(filename), True)
        fptr = open('ref_hsg1.pdbqt')
        reflines = fptr.readlines()
        fptr.close()
        fptr = open(filename)
        testlines = fptr.readlines()
        fptr = open(filename)
        for refline, testline in zip(reflines, testlines):
            # on linux, 0.000 is written -0.000 for atom 1844
            # whereas on sgi, it is written 0.000
            # this is a hack to skip that one character
            #self.assertEqual(refline[:-10], testline[:-10])
            #self.assertEqual(refline[-9:], testline[-9:])
            line1 = refline.strip().split()
            line2 = testline.strip().split()
            for s1, s2 in zip(line1, line2):
                if not s1.isalnum(): self.assertAlmostEqual(float(s1), float(s2), places=2)
                else: self.assertEqual(s1, s2)




if __name__ == '__main__':
    unittest.main()
