autodocktools (1.5.7-4) unstable; urgency=medium

  * Team upload.

  [ Jelmer Vernooĳ ]
  * Remove unnecessary X-Python{,3}-Version field in debian/control.

  [ Andreas Tille ]
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1
  * Remove trailing whitespace in debian/copyright
  * Remove trailing whitespace in debian/rules

 -- Andreas Tille <tille@debian.org>  Wed, 31 Oct 2018 21:52:31 +0100

autodocktools (1.5.7-3) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload
  * Remove myself from Uploaders
  * cme fix dpkg-control (to fix Vcs fields)
  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.2

  [ Steffen Moeller ]
  * debian/upstream/metadata:
    - yamllint cleanliness
    - indicted that there are no entries in software registries

 -- Andreas Tille <tille@debian.org>  Sun, 17 Dec 2017 00:55:14 +0100

autodocktools (1.5.7-2) unstable; urgency=medium

  [ Michael Ott ]
  * Non-maintainer upload.
  * Fixed broken symlink: /usr/bin/autoligand (#857167)

 -- Bastian Blank <waldi@debian.org>  Fri, 17 Mar 2017 20:27:49 +0100

autodocktools (1.5.7-1) unstable; urgency=medium

  * New upstream version (Closes: #767144,#783547,
                          lp:#821299,lp:1386091,lp:#1386163,lp:#1492762)

 -- Steffen Moeller <moeller@debian.org>  Fri, 08 Jul 2016 17:53:47 +0200

autodocktools (1.5.7~rc1+cvs.20140424-1) unstable; urgency=medium

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Thu, 24 Apr 2014 17:40:44 +0200

autodocktools (1.5.7~rc1~cvs.20130917-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Tue, 17 Sep 2013 14:47:39 +0200

autodocktools (1.5.7~rc1~cvs.20130519-1) unstable; urgency=low

  * New upstream version.
  * debian/control: bump standard to 3.9.4 (no changes)
  * debian/control: remove DM-Upload-Allowed
  * debian/copyright: add permission for debian to distribute mgltools

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 19 May 2013 12:00:00 +0200

autodocktools (1.5.6~rc3~cvs.20120206-2) UNRELEASED; urgency=low

  * debian/upstream: Enhanced citation information

 -- Andreas Tille <tille@debian.org>  Sat, 31 Mar 2012 09:11:38 +0200

autodocktools (1.5.6~rc3~cvs.20120206-1) unstable; urgency=low

  * New upstream version.

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 06 Feb 2012 18:00:00 +0100

autodocktools (1.5.6~rc2+cvs.20111222-1) unstable; urgency=low

  * New upstream version.
  * Indentiation error was adopted by upstream, uncommented patch

 -- Steffen Moeller <moeller@debian.org>  Thu, 22 Dec 2011 17:10:07 +0100

autodocktools (1.5.6~rc2+cvs.20110926-2) UNRELEASED; urgency=low

  * Fixed VCS fields in debian/control and debian/README.source

 -- Andreas Tille <tille@debian.org>  Wed, 07 Dec 2011 11:03:54 +0100

autodocktools (1.5.6~rc2+cvs.20110926-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Tue, 27 Sep 2011 10:58:09 +0200

autodocktools (1.5.6~rc1+cvs.20110617-2) UNRELEASED; urgency=low

  * New upload (lp: #623984)
  * Added suggests on mgltools-cadd.
  * Explicitly mentioning the tool AutoLigand. Further work needed.

 -- Steffen Moeller <moeller@debian.org>  Fri, 17 Jun 2011 01:20:14 +0200

autodocktools (1.5.6~rc1+cvs.20110617-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Fri, 17 Jun 2011 01:20:14 +0200

autodocktools (1.5.4.cvs.20100912-3) unstable; urgency=low

  * Added myself to Uploaders
  * Debhelper 8 (control+compat)
  * debian/source/format: 3.0 (quilt)
  * debian/control: Build-Depends: python (>= 2.6) to enable
    python-support2
  * debian/rules: switch to short dh syntax which makes
    dh_python2 easy
    Closes: #616740
  * debian/pycompat: removed
  * debian/rules:
    - Make sure build process will not introduce unwanted versioned
      Python interpreter
    - Clean target remove copy of runAdt which is created in build
      process

 -- Andreas Tille <tille@debian.org>  Wed, 06 Apr 2011 23:08:06 +0200

autodocktools (1.5.4.cvs.20100912-2) unstable; urgency=low

  * Removed dependency on mgltools-scenario2, which will only
    be relevant for the upcoming 1.5.6 version.

 -- Steffen Moeller <moeller@debian.org>  Sun, 26 Sep 2010 23:43:49 +0200

autodocktools (1.5.4.cvs.20100912-1) unstable; urgency=low

  * Added symbolic links to the default installation, i.e.
    autodocktools -> runAdt .
  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Sun, 12 Sep 2010 00:20:31 +0200

autodocktools (1.5.4.cvs.20090603-2) UNRELEASED; urgency=low

  * Documented informations in ‘debian/upstream-metadata.yaml’.

 -- Charles Plessy <plessy@debian.org>  Sun, 03 Jan 2010 22:55:36 +0900

autodocktools (1.5.4.cvs.20090603-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Wed, 03 Jun 2009 11:48:19 +0200

autodocktools (1.5.4.cvs.20090528-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Thu, 28 May 2009 21:41:38 +0200

autodocktools (1.5.4.cvs.20090514-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Thu, 14 May 2009 21:46:30 +0200

autodocktools (1.5.4.cvs.20081126-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Wed, 26 Nov 2008 14:43:08 +0100

autodocktools (1.5.2.cvs.20081109-1) unstable; urgency=low

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Sun, 09 Nov 2008 15:02:28 +0100

autodocktools (1.5.2.cvs.20080731-1) unstable; urgency=low

  * Initial release (Closes: #458811).

 -- Steffen Moeller <moeller@debian.org>  Thu, 31 Jul 2008 21:59:54 +0200
